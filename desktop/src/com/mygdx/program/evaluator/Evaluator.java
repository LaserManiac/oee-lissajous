package com.mygdx.program.evaluator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Evaluator {
	
	private static final List<List<Operator>> operators;
	static {
		operators = new ArrayList<List<Operator>>();
		
		List<Operator> lvl1 = new ArrayList<Operator>();
		lvl1.add(new Operator("+", (a,b) -> {return a+b;}));
		lvl1.add(new Operator("-", (a,b) -> {return a-b;}));
		operators.add(lvl1);
		
		List<Operator> lvl2 = new ArrayList<Operator>();
		lvl2.add(new Operator("*", (a,b) -> {return a*b;}));
		lvl2.add(new Operator("/", (a,b) -> {return a/b;}));
		operators.add(lvl2);
		
		List<Operator> lvl3 = new ArrayList<Operator>();
		lvl3.add(new Operator("^", (a,b) -> {return Math.pow(a, b);}));
		operators.add(lvl3);
	}
	
	private static final HashMap<String, Double> constants;
	static {
		constants = new HashMap<String, Double>();
		constants.put("pi", Math.PI);
		constants.put("e", Math.E);
		
		String pattern = "";
		for(String constant : constants.keySet()) pattern += (pattern.isEmpty()?"(-)?(":"|") + constant;
		pattern += ")";
		constMatchPattern = Pattern.compile(pattern);
	}
	
	private static Pattern bracketsWholeMatchPattern = Pattern.compile("(^-)?\\([^\\(\\)]*\\)$");
	private static Pattern numberMatchPattern = Pattern.compile("[\\+-]?\\d+(\\.\\d*)?");
	private static Pattern constMatchPattern;
	
	private static double evaluate(String expression) {
		
		if(numberMatchPattern.matcher(expression).matches()) return Double.parseDouble(expression);
		
		Matcher constantMatcher = constMatchPattern.matcher(expression);
		if(constantMatcher.matches()) {
			String con = constantMatcher.group(2);
			for(String constant : constants.keySet()) {
				if(constant.equals(con)) {
					return constants.get(con) * ("-".equals(constantMatcher.group(1))?-1:1);
				}
			}
		}
		
		Matcher bracketMatcher = bracketsWholeMatchPattern.matcher(expression);
		if(bracketMatcher.matches()) {
			boolean isNegative = "-".equals(bracketMatcher.group(1));
			return parse(expression.substring(isNegative?2:1, expression.length()-1)) * (isNegative?-1:1);
		}
		
		for(List<Operator> level : operators) {
			for(Operator operator : level) {
				List<String> parts = operator.split(expression);
				if(parts.size() > 1) {
					double a = evaluate(parts.get(0));
					for(int i = 1; i < parts.size(); i++) {
						double b = evaluate(parts.get(i));
						a = operator.evaluate(a, b);
					}
					return a;
				}
			}
		}
		
		throw new RuntimeException("Can't evaluate expression: \"" + expression + "\"");
	}
	
	public static double parse(String expression) {
		/*for(Map.Entry<String, String> constant : constants.entrySet()) {
			if(expression.contains(constant.getKey())) {
				expression = expression.replace(constant.getKey(), constant.getValue());
			}
		}*/
		
		for(List<Operator> level : operators) {
			for(Operator operator : level) {
				if(operator.hasDoubleSymbol(expression)) {
					throw new RuntimeException("Duplicate operator detected");
				}
			}
		}
		
		int n = 0;
		for(int i = 0; i < expression.length(); i++) {
			if(expression.charAt(i)=='(') n++;
			if(expression.charAt(i)==')') n--;
			if(n<-1 || n>1) throw new RuntimeException("Only one level of bracket nesting is allowed");
		}
		if(n != 0) throw new RuntimeException("Mismatched brackets");
		
		expression = expression.replaceAll(" ", "");
		return evaluate(expression);
	}
	
}
