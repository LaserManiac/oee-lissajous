package com.mygdx.program.evaluator;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Operator {
	
	private static final String SPECIAL = "+*^";
	private static boolean isSpecialSymbol(String symbol) {
		return SPECIAL.contains(symbol);
	}
	
	private final Pattern splitPattern, duplicatePattern;
	private final Operation operation;
	
	public String getPattern() {
		return duplicatePattern.pattern();
	}
	
	public Operator(String symbol, Operation operation) {
		if(isSpecialSymbol(symbol)) symbol = "\\"+symbol;
		splitPattern = Pattern.compile("\\(.*?\\)|((?<!^)X(?!$))".replace("X", symbol));
		duplicatePattern = Pattern.compile("X{2,}".replace("X", symbol));
		this.operation = operation;
	}
	
	public boolean hasDoubleSymbol(String expression) {
		return duplicatePattern.matcher(expression).find();
	}
	
	public double evaluate(double a, double b) {
		return operation.execute(a, b);
	}
	
	public List<String> split(String expression) {
		List<String> parts = new ArrayList<String>();
		Matcher matcher = splitPattern.matcher(expression);
		int lastEnd = 0;
		while(matcher.find()) {
			if(matcher.group(1) != null) {
				parts.add(expression.substring(lastEnd, matcher.start(1)));
				lastEnd = matcher.end();
			}
		}
		if(lastEnd != 0) parts.add(expression.substring(lastEnd));
		
		/*
		if(parts.size() > 0) {
			System.out.println("Split \"" + expression + "\" into:");
			for(String part : parts) System.out.println("-> " + part);
		}
		*/
		
		return parts;
	}
	
	public interface Operation {
		public double execute(double a, double b);
	}
	
}
