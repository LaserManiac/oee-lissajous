package com.mygdx.program;

import java.text.NumberFormat;
import java.util.Locale;

import org.lwjgl.opengl.GL11;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.program.graphImage.LissajousImage;
import com.mygdx.program.signalScreen.SignalScreen;
import com.mygdx.program.signalScreen.selectBox.SignalScreenType;

public class Lissajous extends ApplicationAdapter {
	
	public static Skin skin;
	public static SpriteBatch spriteBatch;
	public static ModelBatch modelBatch;
	public static ShapeRenderer renderer;
	public static Stage stage;
	
	public static boolean debug = false;
	public static final boolean DEBUG_ENABLED = false;
	
	public static SignalScreen<?> signal1, signal2;
	private static Cell<?> signalCell1, signalCell2;
	public static LissajousImage graph;
	
	public static void setSignalScreen1(SignalScreen<?> screen) {
		if(signal1 != null) signal1.dispose();
		signal1 = screen;
		signalCell1.setActor(screen);
	}
	public static void setSignalScreen2(SignalScreen<?> screen) {
		if(signal2 != null) signal2.dispose();
		signal2 = screen;
		signalCell2.setActor(screen);
	}
	
	public static void updateGraph() {
		if(graph != null) {
			graph.requestReplot();
			graph.requestRedraw();
		}
	}
	
	private Table table;
	
	@Override
	public void create () {
		skin = new Skin(Gdx.files.internal("ui/uiskin.json"));
		spriteBatch = new SpriteBatch();
		modelBatch = new ModelBatch();
		renderer = new ShapeRenderer();
		stage = new Stage(new ScreenViewport());
		Gdx.input.setInputProcessor(stage);
		
		SignalScreenType.loadTypes();
		
		table = new Table(skin);
		table.setFillParent(true);
		table.pad(8);
		table.top();
		stage.addActor(table);
		
		graph = new LissajousImage();
		graph.setGridBase(10.0);
		
		signalCell1 = table.add()
				.expandX()
				.fillX()
				.prefHeight(Value.percentHeight(0.2f, table))
				.padBottom(8);
		table.row();
		
		signalCell2 = table.add()
			.expandX()
			.fillX()
			.prefHeight(Value.percentHeight(0.2f, table))
			.padBottom(8);
		table.row();
		
		table.add(graph)
			.expandX()
			.fillX()
			.prefHeight(Value.percentHeight(0.6f, table));
		
		table.pack();
		
		SignalScreenType type = SignalScreenType.types.get(0);
		setSignalScreen1(type.createSignalScreen());
		setSignalScreen2(type.createSignalScreen());
	}

	@SuppressWarnings("unused")
	@Override
	public void render () {
		
		if(Gdx.input.isKeyJustPressed(Keys.H) && DEBUG_ENABLED) {
			debug = !debug;
			stage.setDebugAll(debug);
		}
		
		stage.act();
		
		Gdx.gl.glClearColor(0.2f, 0.3f, 0.4f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		stage.draw();
		
		//if(Gdx.input.isKeyJustPressed(Keys.ESCAPE)) Gdx.app.exit();
		//if(debug) System.out.println(Gdx.graphics.getFramesPerSecond());
	}
	
	@Override
	public void resize (int width, int height) {
		Viewport viewport = stage.getViewport();
		viewport.setScreenPosition(0, 0);
		viewport.update(width, height, true);
		spriteBatch.getProjectionMatrix().setToOrtho2D(0, 0, width, height);
		table.invalidateHierarchy();
	}
	
	@Override
	public void dispose () {
		skin.dispose();
		spriteBatch.dispose();
		signal1.dispose();
		signal2.dispose();
		graph.dispose();
	}
	
	public static void checkForGlErrors(String tag) {
		int error = GL11.glGetError();
		if(error != 0) {
			String err = "";
			switch(error) {
				case 1280: err = "GL_INVALID_ENUM"; break;
				case 1281: err = "GL_INVALID_VALUE"; break;
				case 1282: err = "GL_INVALID_OPERATION"; break;
				case 1283: err = "GL_STACK_OVERFLOW"; break;
				case 1284: err = "GL_STACK_UNDERFLOW"; break;
				case 1285: err = "GL_OUT_OF_MEMORY"; break;
			}
			System.out.println("[OpenGL ERROR " + error + ": " + err + "] " + tag);
		}
	}
	
	private static final NumberFormat NUMBER_FORMAT;
	static {
		NUMBER_FORMAT = NumberFormat.getNumberInstance(Locale.UK);
		NUMBER_FORMAT.setGroupingUsed(false);
		NUMBER_FORMAT.setMinimumFractionDigits(0);
		NUMBER_FORMAT.setMaximumFractionDigits(12);
	}
	public static String doubleToString(double n) {
		return NUMBER_FORMAT.format(n);
	}
	
}
