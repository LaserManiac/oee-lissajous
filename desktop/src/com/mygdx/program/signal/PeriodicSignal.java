package com.mygdx.program.signal;

public abstract class PeriodicSignal extends Signal {
	
	public PeriodicSignal() {
		addParameter("period", new Parameter(2));
		addParameter("amplitude", new Parameter(1));
		addParameter("phase", new Parameter(0));
	}
	
	@Override
	public boolean isPeriodic() {
		return true;
	}
	
	@Override
	public double providePlotInterval() {
		return get("period");
	}
	
	@Override
	public double provideCenterX() {
		return 0; //get("phase");
	}
	
	@Override
	public double provideCenterY() {
		return get("y_offset");
	}
	
	@Override
	public double provideCenterWidth() {
		return get("period");
	}
	
	@Override
	public double provideCenterHeight() {
		return get("amplitude")*2;
	}
	
	
}
