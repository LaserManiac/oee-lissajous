package com.mygdx.program.signal;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.MathUtils;

public class OldComplexSignal extends Signal {

	private List<Long> indexer;
	private long indexCounter;
	
	/**
	 * Name For Id
	 */
	public String nfi(String name, long id) {
		return name + "_" + id;
	}
	
	public OldComplexSignal() {
		indexer = new ArrayList<Long>();
		indexCounter = 0;
		
		addParameter("amplitude", new Parameter(1.0));
		addParameter("segments", new Parameter(0));
		addParameter("start", new Parameter(0));
		
		addSegment(0);
	}
	
	public long addSegment(int index) {
		int id;
		int segments = (int) get("segments");
		if(index > segments) id = segments;
		else id = index;
		
		set("segments", segments + 1);
		
		indexer.add(index, indexCounter++);
		addParameter(nfi("size", id), new Parameter(1.0));
		addParameter(nfi("start", id), new Parameter(0.0));
		addParameter(nfi("end", id), new Parameter(1.0));
		addParameter(nfi("ease", id), new Parameter(Ease.LINEAR.index()));
		
		return id;
	}
	
	public void removeSegment(int index) {
		int segments = (int) get("segments");
		if(index >= segments) throw new RuntimeException("Can't remove non existing segment! (index " + index + ")");
		
		set("segments", segments - 1);
		
		long id = indexer.remove(index);
		removeParameter(nfi("size", id));
		removeParameter(nfi("start", id));
		removeParameter(nfi("end", id));
		removeParameter(nfi("ease", id));
	}

	public double getTotalWidth() {
		double r = 0;
		int segments = (int) get("segments");
		for(int i = 0; i < segments; i++) {
			long index = indexer.get(i);
			r += get(nfi("size", index));
		}
		return r;
	}
	
	@Override
	public double evaluate(double t) {
		final double defaultValue = -10.0;
		
		if(t < 0) return defaultValue;
		
		int segments = (int) get("segments");
		
		double amplitude = get("amplitude");
		double yOffset = get("y_offset");
		
		double total = 0;
		for(int i = 0; i < segments; i++) {
			long index = indexer.get(i);
			double size = get(nfi("size", index));
			if(t < total + size) {
				double local = t - total;
				double percent = local/size;
				
				double start = get(nfi("start", index));
				double end = get(nfi("end", index));
				
				Ease ease = Ease.values()[(int) get(nfi("ease", index))];
				double interpolated_pc = ease.interpolator.apply(percent);
				double value = start + (end - start)*interpolated_pc;
				double r = value * amplitude;
				return yOffset + clampToMinMax(r);
				
			} else {
				total += size;
			}
		}
		
		return defaultValue;
	}

	public static enum Ease {
		LINEAR("Linearno", (x) -> { return x; }),
		SINE("Sinusno", (x) -> { return (1 - Math.cos(x * MathUtils.PI)) / 2; } );
		
		public final Interpolator interpolator;
		public final String name;
		
		private Ease(String name, Interpolator interpolator) {
			this.name = name;
			this.interpolator = interpolator;
		}
		
		public int index() {
			return ordinal();
		}
	}
	
	private static interface Interpolator {
		public double apply(double x);
	}
	
	@Override
	public boolean isPeriodic() {
		return false;
	}
	
	@Override
	public double providePlotInterval() {
		return getTotalWidth();
	}

	@Override
	public double provideCenterX() {
		return 0;
	}

	@Override
	public double provideCenterY() {
		return get("y_offset");
	}

	@Override
	public double provideCenterWidth() {
		return getTotalWidth();
	}

	@Override
	public double provideCenterHeight() {
		return get("amplitude")*2;
	}

}
