package com.mygdx.program.signal;

public class SineExpSignal extends Signal {
	
	public SineExpSignal() {
		addParameter("period", new Parameter(2));
		addParameter("amplitude", new Parameter(1));
		addParameter("phase", new Parameter(0));
		addParameter("exp", new Parameter(0.1));
		addParameter("exp_shift", new Parameter(0));
		addParameter("plot", new Parameter(4));
	}
	
	@Override
	public double evaluate(double t) {
		double amplitude = get("amplitude");
		double period = get("period");
		double phase = get("phase");
		double yOffset = get("y_offset");
		double exp = get("exp");
		double exp_shift = get("exp_shift");
		
		double w = 360/period;
		double r = amplitude*Math.sin(Math.toRadians(w*t + phase)) * Math.exp(exp*(t - exp_shift));
		return yOffset + clampToMinMax(r);
	}

	@Override
	public boolean isPeriodic() {
		return false;
	}
	
	@Override
	public double providePlotInterval() {
		return get("plot");
	}

	@Override
	public double provideCenterX() {
		return 0;
	}

	@Override
	public double provideCenterY() {
		return get("y_offset");
	}

	@Override
	public double provideCenterWidth() {
		return get("plot");
	}

	@Override
	public double provideCenterHeight() {
		return get("amplitude")*2;
	}
	
}
