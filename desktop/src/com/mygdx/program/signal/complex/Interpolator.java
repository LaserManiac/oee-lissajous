package com.mygdx.program.signal.complex;

public enum Interpolator {
	
	LINEAR("Linearan", (x) -> x ),
	SINE("Sinus", (x) -> (1 + Math.cos(Math.PI*(1 + x))) / 2 ),
	SINEIN("Sinus Ulaz", (x) -> Math.sin(Math.PI*(1.5 + x/2)) + 1),
	SINEOUT("Sinus Izlaz", (x) -> Math.sin(Math.PI/2*x)),
	EXPIN("Exp. Ulaz", (x) -> (Math.pow(Math.E, x*Interpolator.EXP_FACTOR) - 1) / (Math.pow(Math.E, Interpolator.EXP_FACTOR) - 1)),
	EXPOUT("Exp. Izlaz", (x) -> 1.0 - (Math.pow(Math.E, (1.0 - x)*Interpolator.EXP_FACTOR ) - 1) / (Math.pow(Math.E, Interpolator.EXP_FACTOR) - 1));
	
	private static final int EXP_FACTOR = 5;
	
	private final InterpolatorRunnable runnable;
	public final String name;
	
	public double apply(double x) {
		return runnable.apply(x);
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	private Interpolator(String name, InterpolatorRunnable runnable) {
		this.runnable = runnable;
		this.name = name;
	}
	
	private interface InterpolatorRunnable {
		public double apply(double x);
	}
	
}
