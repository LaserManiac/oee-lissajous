package com.mygdx.program.signal.complex;

import com.mygdx.program.signal.Parameter;

public class Segment {
	
	public Interpolator interpolator = Interpolator.LINEAR;
	public final Parameter start = new Parameter(0.0);
	public final Parameter end = new Parameter(1.0);
	public final Parameter width = new Parameter(1.0);
	
	public double evaluate(double x) {
		if(x < 0) return 0.0;
		
		double width = this.width.get();
		if(x > width) return 0.0;
		
		double start = this.start.get();
		double end = this.end.get();
		
		double a = interpolator.apply(x/width);
		return start + a*(end-start);
	}
	
}
