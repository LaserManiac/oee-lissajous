package com.mygdx.program.signal.complex;

import java.util.ArrayList;
import java.util.List;

import com.mygdx.program.signal.Parameter;
import com.mygdx.program.signal.Signal;

public class ComplexSignal extends Signal {
	
	private List<Segment> segments = new ArrayList<Segment>();
	public int getNumOfSegments() {
		return segments.size();
	}
	public Segment addSegment(int index) {
		Segment segment = new Segment();
		segments.add(index, segment);
		set("period", calculateTotalWidth());
		return segment;
	}
	public Segment removeSegment(int index) {
		Segment segment = segments.remove(index);
		calculateTotalWidth();
		set("period", calculateTotalWidth());
		return segment;
	}
	public Segment getSegment(int index) {
		return segments.get(index);
	}
	public int indexOfSegment(Segment segment) {
		return segments.indexOf(segment);
	}
	public double positionOfSegment(int index) {
		double posX = 0;
		for(int i = 0; i < index; i++) posX += segments.get(i).width.get();
		return posX;
	}
	
	public Segment getSegmentAt(double t) {
		double period = get("period");
		
		if(!periodic && (t < 0 || t > period)) return null;
		
		t = t % period;
		if(t < 0) t = period + t;
		
		if(t < 0) return null;
		double total = 0;
		for(Segment segment : segments) {
			if(t <= total + segment.width.get()) {
				return segment;
			} else {
				total += segment.width.get();
			}
		}
		return null;
	}
	
	private boolean periodic = false;
	public void setPeriodic(boolean periodic) {
		this.periodic = periodic;
	}
	@Override
	public boolean isPeriodic() {
		return periodic;
	}
	
	public ComplexSignal() {
		addParameter("amplitude", new Parameter(1.0));
		addParameter("period", new Parameter(0.0));
	}
	
	public double calculateTotalWidth() {
		double totalWidth = 0;
		for(Segment segment : segments) totalWidth += segment.width.get();
		return totalWidth;
	}
	
	@Override
	public void plot(double start, double end, int steps) {
		calculateTotalWidth();
		super.plot(start, end, steps);
	}
	
	@Override
	public double evaluate(double t) {
		double period = get("period");
		
		if(!periodic && (t < 0 || t > period)) return 0.0;
		
		t = t % period;
		if(t < 0) t = period + t;
		
		double total = 0;
		for(Segment segment : segments) {
			if(t <= total + segment.width.get()) {
				double amplitude = get("amplitude");
				double yOffset = get("y_offset");
				return yOffset + clampToMinMax(segment.evaluate(t - total) * amplitude);
			} else {
				total += segment.width.get();
			}
		}
		
		return 0.0;
	}

	public double providePlotInterval() {
		return get("period");
	}

	@Override
	public double provideCenterX() {
		return 0;	
	}

	@Override
	public double provideCenterY() {
		return get("y_offset");
	}

	@Override
	public double provideCenterWidth() {
		return get("period");
	}

	@Override
	public double provideCenterHeight() {
		return get("amplitude")*2;
	}
	
}
