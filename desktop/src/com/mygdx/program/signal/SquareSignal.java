package com.mygdx.program.signal;

public class SquareSignal extends PeriodicSignal {

	public SquareSignal() {
		super();
		addParameter("split", new Parameter(0.5));
	}
	
	@Override
	public double evaluate(double t) {
		double amplitude = get("amplitude");
		double period = get("period");
		double phase = get("phase");
		double split = get("split");
		double yOffset = get("y_offset");
		
		double xOff = phase/360*period;
		double middle = split*period;
		
		double periodPart = (t+xOff) % period;
		double periodLocation = periodPart<0 ? period+periodPart : periodPart;
		double r = periodLocation<middle ? amplitude : -amplitude;
		
		return yOffset + clampToMinMax(r);
	}

}
