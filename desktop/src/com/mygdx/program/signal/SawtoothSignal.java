package com.mygdx.program.signal;

public class SawtoothSignal extends PeriodicSignal {

	public SawtoothSignal() {
		super();
		addParameter("tooth", new Parameter(0.5));
	}

	@Override
	public double evaluate(double t) {
		double amplitude = get("amplitude");
		double period = get("period");
		double phase = get("phase");
		double yOffset = get("y_offset");
		double tooth = get("tooth");
		
		double toothPos = tooth*period;
		double xOff = -phase/360*period;
		
		double rt = (t-xOff) % period;
		if(rt < 0) rt = period+rt;
		double r;
		
		if(rt < toothPos) {
			r = (rt/toothPos*2-1)*amplitude;
		} else {
			r = (1-(rt-toothPos)/(period-toothPos)*2)*amplitude;
		}
		return yOffset + clampToMinMax(r);
	}

}
