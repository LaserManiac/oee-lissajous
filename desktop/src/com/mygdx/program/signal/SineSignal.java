package com.mygdx.program.signal;

public class SineSignal extends PeriodicSignal {
	
	@Override
	public double evaluate(double t) {
		double amplitude = get("amplitude");
		double period = get("period");
		double phase = get("phase");
		double yOffset = get("y_offset");
		
		double w = 360/period;
		double r = amplitude*Math.sin(Math.toRadians(w*t + phase));
		return yOffset + clampToMinMax(r);
	}
	
}
