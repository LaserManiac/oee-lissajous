package com.mygdx.program.signal;

import java.util.List;

import com.mygdx.program.expression.Expression;
import com.mygdx.program.expression.ExpressionContext;
import com.mygdx.program.expression.Variable;
import com.mygdx.program.signalScreen.expression.ParameterBlueprint;

public class ExpressionSignal extends Signal implements ExpressionContext {
	
	private final Expression expression;
	private final Expression period;

	private final List<Variable> variables;
	
	//private double minHeight = -1, maxHeight = 1;
	
	public ExpressionSignal(Expression expression, Expression period, List<ParameterBlueprint> parameters, List<Variable> variables) {
		super();
		this.expression = expression;
		this.period = period;
		this.variables = variables;
		
		parameters.forEach((blueprint) -> addParameter(blueprint.name, new Parameter(blueprint.initial)));
		
		if(!isPeriodic()) addParameter("plot", new Parameter(1.0));
	}
	
	public void begin() {
		expression.linkContext(this);
		variables.forEach((var) -> var.update(this));
		//System.out.println("Begin");
	}
	
	public void end() {
		expression.linkContext(null);
		//System.out.println("End");
	}
	
	/*
	@Override
	public void plot(double start, double end, int steps) {
		polygon = new double[(steps+1)*2];
		double step = (end-start)/steps;
		for(int i = 0; i <= steps; i++) {
			double l = i*step;
			double t = start + l;
			double v = Math.max(Math.min(evaluate(t), 10000), -10000);
			polygon[i*2+0] = t;
			polygon[i*2+1] = v;
			
			if(i == 0) {
				minHeight = v;
				maxHeight = v;
			} else {
				minHeight = Math.min(minHeight, v);
				maxHeight = Math.max(maxHeight, v);
			}
		}
	}
	*/
	
	private Parameter argument = new Parameter(0.0);
	@Override
	public Parameter getArgument() {
		return argument;
	}
	
	@Override
	public double getPeriod() {
		period.linkContext(this);
		return period.evaluate();
	}
	
	@Override
	public boolean isPeriodic() {
		return period != null;
	}

	@Override
	public double evaluate(double t) {
		if(isPeriodic()) {
			double period = getPeriod();
			t = t % period;
			if(t < 0) t = period + t;
		}
		argument.set(t);
		return get("y_offset") + clampToMinMax(expression.evaluate());
	}

	@Override
	public double providePlotInterval() {
		return isPeriodic() ? getPeriod() : get("plot");
	}

	@Override
	public double provideCenterX() {
		return 0;
	}

	@Override
	public double provideCenterY() {
		return 0;
	}

	@Override
	public double provideCenterWidth() {
		return providePlotInterval();
	}

	@Override
	public double provideCenterHeight() {
		return 2; //Math.min(maxHeight-minHeight, 1000);
	}

}
