package com.mygdx.program.signal;

import com.mygdx.program.Lissajous;

public class Parameter {
	private double value;
	public double get() {
		return value;
	}
	public void set(double value) {
		this.value = value;
		Lissajous.updateGraph();
	}
	
	public Parameter(double value) {
		this.value = value;
	}
}
