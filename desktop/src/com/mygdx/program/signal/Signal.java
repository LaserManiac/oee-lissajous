package com.mygdx.program.signal;

import java.util.HashMap;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public abstract class Signal {
	
	protected double[] polygon;
	public double[] getPolygon() {
		return polygon;
	}
	
	protected HashMap<String, Parameter> parameters = new HashMap<String, Parameter>();
	public HashMap<String, Parameter> getParameters() {
		return parameters;
	}
	public void addParameter(String name, Parameter parameter) {
		parameters.put(name, parameter);
	}
	public void removeParameter(String name) {
		parameters.remove(name);
	}
	public Parameter getParameter(String name) {
		return parameters.get(name);
	}
	public double get(String name) {
		return getParameter(name).get();
	}
	public void set(String name, double value) {
		getParameter(name).set(value);
	}
	
	public abstract boolean isPeriodic();
	
	protected double clampToMinMax(double t) {
		double min = get("min");
		double max = get("max");
		return Math.max(Math.min(t, max), min);
	}
	
	public Signal() {
		addParameter("y_offset", new Parameter(0));
		addParameter("min", new Parameter(-10000));
		addParameter("max", new Parameter(10000));
	}
	
	public abstract double evaluate(double t);
	
	public abstract double providePlotInterval();
	
	public abstract double provideCenterX();
	public abstract double provideCenterY();
	public abstract double provideCenterWidth();
	public abstract double provideCenterHeight();
	
	public void plot(double start, double end, int steps) {
		polygon = new double[(steps+1)*2];
		double step = (end-start)/steps;
		for(int i = 0; i <= steps; i++) {
			double l = i*step;
			double t = start + l;
			polygon[i*2+0] = t;
			polygon[i*2+1] = evaluate(t);
		}
	}
	
	public void render(ShapeRenderer renderer) {
		int size = polygon.length/2;
		for(int i = 0; i < size-1; i++) {
			float x1 = (float)(polygon[i*2+0]);
			float y1 = (float)(polygon[i*2+1]);
			float x2 = (float)(polygon[i*2+2]);
			float y2 = (float)(polygon[i*2+3]);
			renderer.line(x1, y1, x2, y2);
		}
	}
	
}
