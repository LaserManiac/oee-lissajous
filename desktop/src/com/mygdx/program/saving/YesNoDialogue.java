package com.mygdx.program.saving;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.program.Lissajous;

public abstract class YesNoDialogue extends Window {

	public YesNoDialogue(String title) {
		super("", Lissajous.skin, "plain");

		setModal(true);
		
		setWidth(250);
		setHeight(50);
		pad(5);
		
		Label titleLabel = new Label(title, Lissajous.skin, "trans");
		titleLabel.setAlignment(Align.center);
		add(titleLabel).expandX().fillX().colspan(2);
		row();
		
		TextButton yes = new TextButton("DA", Lissajous.skin, "light");
		yes.padBottom(6).padTop(-2);
		add(yes).fillX().padRight(2);
		yes.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				yes();
				getStage().getRoot().removeActor(YesNoDialogue.this);
			}
		});
		
		TextButton no = new TextButton("NE", Lissajous.skin, "light");
		no.padBottom(6).padTop(-2);
		add(no).fillX().padLeft(2);
		no.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				no();
				getStage().getRoot().removeActor(YesNoDialogue.this);
			}
		});
		
	}
	
	@Override
	public void act(float delta) {
		setPosition(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2, Align.center);
	}
	
	public abstract void yes();
	public abstract void no();
	
}
