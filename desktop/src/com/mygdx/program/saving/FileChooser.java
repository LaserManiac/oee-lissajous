package com.mygdx.program.saving;

import java.io.File;
import java.io.FileFilter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.program.Lissajous;

public class FileChooser extends Window {

	private final String directory;
	private final String extension;
	
	private final Table items;
	private final ScrollPane scroll;
	private ButtonGroup<SignalButton> radio;
	
	public FileChooser(boolean save, String directory, String extension, final FileChooserRunnable runnable) {
		super("", Lissajous.skin, "plain");
		this.directory = directory;
		this.extension = extension;
		
		setWidth(180);
		setHeight(200);
		
		setModal(true);
		addListener(new InputListener() {
			@Override
			public boolean keyDown (InputEvent event, int keycode) {
				if(keycode == Keys.ESCAPE) {
					getStage().getRoot().removeActor(FileChooser.this);
					return true;
				}
				return false;
			}
		});
		
		items = new Table(Lissajous.skin);
		items.top();
		
		scroll = new ScrollPane(items, Lissajous.skin, "blue");
		scroll.setupFadeScrollBars(0.1f, 1.0f);
		scroll.setScrollingDisabled(true, false);
		scroll.setFadeScrollBars(false);
		scroll.setFlickScroll(false);
		scroll.setOverscroll(false, false);
		
		pad(1);
		row();
		add(scroll).expand().fill().padTop(4).padRight(4).colspan(2);
		row();
		
		final TextField name;
		if(save) {
			
			final TextButton saveButton = new TextButton("SPREMI", Lissajous.skin, "darker");
			
			name = new TextField("bezimeni", Lissajous.skin, "darker");
			name.setAlignment(Align.center);
			name.setMaxLength(50);
			name.setProgrammaticChangeEvents(true);
			name.setTextFieldFilter(new TextFieldFilter() {
				@Override
				public boolean acceptChar(TextField textField, char c) {
					return Character.isLetterOrDigit(c) || "_-()[] ".contains(""+c);
				}
			});
			name.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					if(((TextField)actor).getText().isEmpty()) {
						saveButton.setDisabled(true);
					} else {
						saveButton.setDisabled(false);
					}
				}
			});
			add(name).expandX().fillX().pad(4).padBottom(0).colspan(2);
			row();
			
			saveButton.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					FileHandle file = Gdx.files.external(directory + name.getText() + extension);
					if(file.exists()) {
						getStage().addActor(new YesNoDialogue("PREBRISI POSTOJECU DATOTEKU?") {
							@Override
							public void yes() {
								runnable.choose(file);
								getStage().getRoot().removeActor(FileChooser.this);
							}
							@Override
							public void no() { }
						});
					} else {
						runnable.choose(file);
						getStage().getRoot().removeActor(FileChooser.this);
					}
				}
			});
			add(saveButton).fillX().pad(4);
			
		} else {
			
			name = null;
			TextButton loadButton = new TextButton("UCITAJ", Lissajous.skin, "darker");
			loadButton.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					if(radio.getChecked() == null) return;
					runnable.choose(radio.getChecked().getFile());
					getStage().getRoot().removeActor(FileChooser.this);
				}
			});
			add(loadButton).fillX().pad(4);
	
		}
		
		TextButton deleteButton = new TextButton("OBRISI", Lissajous.skin, "darker");
		deleteButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(radio.getChecked() == null) return;
				getStage().addActor(new YesNoDialogue("OBRISI ODABRANU DATOTEKU?") {
					@Override
					public void yes() {
						SignalButton button = radio.getChecked();
						button.file.delete();
						radio.clear();
						items.clear();
						updateItems(null);
					}
					@Override
					public void no() { }
				});
			}
		});
		add(deleteButton).fillX().padRight(4);
		
		updateItems(name);
		
		addListener(new InputListener() {
			@Override
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				if(x<0 || y<0 || x>getWidth() || y>getHeight()) {
					getStage().getRoot().removeActor(FileChooser.this);
				}
				return true;
			}
		});
	}
	
	private void updateItems(TextField name) {
		FileHandle handle = Gdx.files.external(directory);
		FileHandle[] children = handle.list(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith(extension);
			}
		});
			
		radio = new ButtonGroup<SignalButton>();
		radio.setMinCheckCount(0);
		
		if(children.length > 0) {
			for(FileHandle file : children) {
				SignalButton signal = new SignalButton(file);
				if(name != null) signal.addListener(new ChangeListener() {
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						name.setText(((SignalButton)actor).getText().toString());
					}
				});
				items.add(signal).minWidth(1).expandX().fillX().pad(4).padTop(0);
				items.row();
				radio.add(signal);
			}
		} else {
			Label l1 = new Label("Nema spremljenih", Lissajous.skin, "trans");
			l1.setAlignment(Align.center);
			Label l2 = new Label("datoteka", Lissajous.skin, "trans");
			l2.setAlignment(Align.center);
			items.add(l1).expandX().fillX().padLeft(4).padRight(4);
			items.row();
			items.add(l2).expandX().fillX().padLeft(4).padRight(4);
		}
	}
	
	@Override
	public void act(float delta) {
		setPosition(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2, Align.center);
		super.act(delta);
	}
	
	@Override
	public void setStage(Stage stage) {
		super.setStage(stage);
		if(stage != null) {
			stage.setKeyboardFocus(this);
			stage.setScrollFocus(scroll);
		}
	}
	
	private class SignalButton extends TextButton {
		
		private final FileHandle file;
		public FileHandle getFile() {
			return file;
		}
		
		public SignalButton(FileHandle file) {
			super("", Lissajous.skin, "list");
			this.file = file;
			padLeft(4);
			getLabel().setAlignment(Align.left);
			getLabelCell().minWidth(1).padRight(4).left();
			getLabel().setEllipsis("...");
			setText(file.nameWithoutExtension());
		}
		
		@Override
		public void sizeChanged() {
			super.sizeChanged();
			if(this.file != null) {
				getLabel().setWidth(getWidth());
				getLabel().setText(file.nameWithoutExtension());
			}
		}
		
	}
	
	public interface FileChooserRunnable {
		public void choose(FileHandle file);
	}
	
}
