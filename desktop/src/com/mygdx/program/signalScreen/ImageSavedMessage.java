package com.mygdx.program.signalScreen;

import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.program.Lissajous;

public class ImageSavedMessage extends Window {
	
	public ImageSavedMessage(String title, final FileHandle file) {
		super("", Lissajous.skin, "plain");

		setModal(true);
		
		setWidth(250);
		pad(5);
		
		Label titleLabel = new Label(title, Lissajous.skin, "trans");
		titleLabel.setAlignment(Align.center);
		add(titleLabel).expandX().fillX().colspan(2).padTop(-4).padBottom(2);
		row();
		
		TextButton folder = new TextButton("FOLDER", Lissajous.skin, "light");
		add(folder).expandX().fillX();
		folder.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				try {
					//System.out.println("Opening: " + file.path());
					Runtime.getRuntime().exec("explorer.exe /select, " + file.file().getAbsolutePath());
				} catch(IOException e) {
					System.out.println("ERROR opening explorer.exe at \"" + file.file().getAbsolutePath() + "\"");
				} finally {
					getStage().getRoot().removeActor(ImageSavedMessage.this);
				}
			}
		});
		row();
		
		TextButton yes = new TextButton("OK", Lissajous.skin, "light");
		add(yes).expandX().fillX().padTop(4);
		yes.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				getStage().getRoot().removeActor(ImageSavedMessage.this);
			}
		});
		
		pack();
		
	}
	
	@Override
	public void act(float delta) {
		setPosition(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2, Align.center);
	}
}
