package com.mygdx.program.signalScreen.complex;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.mygdx.program.Lissajous;
import com.mygdx.program.graphImage.SignalImage;
import com.mygdx.program.resizeWidget.LineWidget;
import com.mygdx.program.resizeWidget.LineWidget.Axis;
import com.mygdx.program.resizeWidget.ResizeWidget;
import com.mygdx.program.signal.complex.ComplexSignal;
import com.mygdx.program.signal.complex.Interpolator;
import com.mygdx.program.signal.complex.Segment;
import com.mygdx.program.signalScreen.NumericTextfield;
import com.mygdx.program.signalScreen.SignalScreen;

public class ComplexSignalScreen extends SignalScreen<ComplexSignal> {

	private final Color WIDGET_COLOR = Color.LIME;
	
	private final NumericTextfield fPeriod;
	/** Only used as an anti feedback-loop hack */
	public void updatePeriodTextfieldWhenSegmentResized(double period) {
		String str = Double.toString(period);
		if(!fPeriod.getText().equals(str)) {
			fPeriod.setProgrammaticChangeEvents(false);
			fPeriod.setText(str);
			fPeriod.setValue(period);
			fPeriod.setProgrammaticChangeEvents(true);
		}
	}
	
	private final TextButton periodicButton;
	public void setPeriodic(boolean periodic) {
		periodicButton.setText(periodic ? "PERIODICAN" : "NEPERIODICAN");
		signal.setPeriodic(periodic);
	}
	
	public SignalImage getImage() {
		return image;
	}
	
	private final Table segmentUITable;
	private List<SegmentUI> segmentUIList = new ArrayList<SegmentUI>();
	private void rebuildSegmentUI() {
		segmentUITable.clearChildren();
		int i = 1;
		for(SegmentUI ui : segmentUIList) {
			ui.setTitleIndex(i++);
			segmentUITable.add(ui).expandX().fillX().padBottom(4).padTop(12);
			segmentUITable.row();
		}
	}
	
	public SegmentUI addSegment(int index) {
		Segment segment = signal.addSegment(index);
		SegmentUI ui = new SegmentUI(this, segment);
		segmentUIList.add(index, ui);
		rebuildSegmentUI();
		updateFields();
		image.requestReplot();
		image.requestRedraw();
		return ui;
	}
	
	public void removeSegment(Segment segment) {
		int index = signal.indexOfSegment(segment);
		signal.removeSegment(index);
		SegmentUI ui = segmentUIList.remove(index);
		for(ResizeWidget widget : ui.getWidgets()) image.removeActor(widget);
		for(NumericTextfield field : ui.getFields()) unregisterField(field);
		rebuildSegmentUI();
		updateFields();
		image.requestReplot();
		image.requestRedraw();
	}
	
	public ComplexSignalScreen() {
		
		image.addCaptureListener(new InputListener() {
			@Override
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				double worldX = image.toWorldX(image.getX() + x);
				//System.out.println("Cicked at " + worldX);
				Segment segment = signal.getSegmentAt(worldX);
				if(segment != null) {
					//System.out.println("Segment is not null!");
					for(SegmentUI ui : segmentUIList) {
						if(ui.getSegment() == segment) {
							ui.getStage().setKeyboardFocus(null);
							ui.getStage().setKeyboardFocus(ui);
							return true;
						}
					}
				}
				return false;
			}
		});
		
		Table control = new Table();
		
		TextButton add = new TextButton("DODAJ", Lissajous.skin, "light");
		add.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				/*SegmentUI ui =*/ addSegment(signal.getNumOfSegments());
				//ui.getStage().setKeyboardFocus(ui);
			}
		});
		add.getLabel().setEllipsis("...");
		add.getLabel().setAlignment(Align.left);
		add.getLabelCell().minWidth(1).padLeft(4).padRight(4).left();
		add.setClip(true);
		control.add(add).expandX().fillX().minWidth(1).padTop(4).prefWidth(Value.percentWidth(0.5f, control));
		
		periodicButton = new TextButton(signal.isPeriodic() ? "PERIODICAN" : "NEPERIODICAN", Lissajous.skin, "light");
		periodicButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				boolean p = !signal.isPeriodic();
				setPeriodic(p);
				image.requestReplot();
				image.requestRedraw();
			}
		});
		periodicButton.getLabel().setEllipsis("...");
		periodicButton.getLabel().setAlignment(Align.left);
		periodicButton.getLabelCell().minWidth(1).padLeft(4).padRight(4).left();
		periodicButton.setClip(true);
		control.add(periodicButton).expandX().fillX().minWidth(1).padTop(4).padLeft(4).prefWidth(Value.percentWidth(0.5f, control));
		

		NumericTextfield fAmplitude = new NumericTextfield(image, signal.getParameter("amplitude"));
		new LineWidget(fAmplitude, WIDGET_COLOR, Axis.Y) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				return worldY - signal.get("y_offset");
			}
			@Override
			public void adjustWorldPosition(double x) {
				worldY = x + signal.get("y_offset");
			}
		};
		addField("Amplituda", fAmplitude);
		
		fPeriod = new NumericTextfield(image, signal.getParameter("period"));
		fPeriod.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				double old = signal.calculateTotalWidth();
				double _new = signal.get("period");
				//System.out.println("New/Old: " + _new + "/" + old);
				for(int i = 0; i < signal.getNumOfSegments(); i++) {
					Segment segment = signal.getSegment(i);
					double newWidth = segment.width.get()*_new/old;
					segment.width.set(newWidth);
					segmentUIList.get(i).updateWidthWhenPeriodChanged(newWidth);
				}
			}
		});
		addField("Sirina/period", fPeriod);
		
		
		titleTable.row();
		titleTable.add(control).expandX().fillX();
		
		segmentUITable = new Table();
		fieldTable.add(segmentUITable).colspan(3).expandX().fillX();
		fieldTable.row();
		
		addSegment(0);
		
	}
	
	@Override
	protected ComplexSignal createSignal() {
		return new ComplexSignal();
	}
	
	@Override
	public void toJson(Json json) {
		super.toJson(json);
		json.writeValue("periodic", signal.isPeriodic());
		
		json.writeArrayStart("segments");
		for(int i = 0; i < signal.getNumOfSegments(); i++) {
			Segment segment = signal.getSegment(i);
			json.writeObjectStart();
			json.writeValue("width", segment.width.get());
			json.writeValue("start", segment.start.get());
			json.writeValue("end", segment.end.get());
			json.writeValue("interpolator", segment.interpolator.name());
			json.writeObjectEnd();
		}
		json.writeArrayEnd();
	}
	
	public static void loadSegments(ComplexSignalScreen screen, JsonValue segments) {
		try {
			
			ComplexSignal signal = screen.getSignal();
			int i = 0;
			for(JsonValue segment = segments.child; segment != null; segment = segment.next) {
				//System.out.println("NumOfSegments: " + (i+1));
				Segment seg;
				if(i == 0) {
					seg = signal.getSegment(0);
					i++;
				} else {
					screen.addSegment(i);
					seg = signal.getSegment(i++);
				}
				
				double width = segment.getDouble("width");
				double start = segment.getDouble("start");
				double end = segment.getDouble("end");
				String inpStr = segment.getString("interpolator");
				Interpolator interpolator = Interpolator.valueOf(inpStr);
				
				seg.width.set(width);
				seg.start.set(start);
				seg.end.set(end);
				seg.interpolator = interpolator;
			}
			
		} catch(Exception e) {
			System.out.println("ERORR loading complex signal segments: " + e.getMessage());
		}
	}

}
