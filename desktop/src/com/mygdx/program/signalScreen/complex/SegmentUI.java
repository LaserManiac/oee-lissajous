package com.mygdx.program.signalScreen.complex;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.program.Lissajous;
import com.mygdx.program.resizeWidget.CircleWidget;
import com.mygdx.program.resizeWidget.LineWidget;
import com.mygdx.program.resizeWidget.LineWidget.Axis;
import com.mygdx.program.resizeWidget.ResizeWidget;
import com.mygdx.program.signal.complex.ComplexSignal;
import com.mygdx.program.signal.complex.Interpolator;
import com.mygdx.program.signal.complex.Segment;
import com.mygdx.program.signalScreen.NumericTextfield;

public class SegmentUI extends Table {
	
	private final Color RESIZE_WIDGET_COLOR = Color.ORANGE.cpy().mul(0.8f, 0.8f, 0.8f, 1.0f);

	private List<ResizeWidget> widgets = new ArrayList<ResizeWidget>();
	public List<ResizeWidget> getWidgets() {
		return widgets;
	}
	
	private List<NumericTextfield> fields = new ArrayList<NumericTextfield>();
	public List<NumericTextfield> getFields() {
		return fields;
	}
	
	private final NumericTextfield fWidth;
	/** Only used as an anti feedback-loop hack */
	public void updateWidthWhenPeriodChanged(double newWidth) {
		String str = Double.toString(newWidth);
		if(!fWidth.getText().equals(str)) {
			fWidth.setProgrammaticChangeEvents(false);
			fWidth.setText(str);
			fWidth.setValue(newWidth);
			fWidth.setProgrammaticChangeEvents(true);
		}
	}
	
	private final Segment segment;
	public Segment getSegment() {
		return segment;
	}
	
	private final ComplexSignalScreen screen;
	
	private Label titleLabel;
	public void setTitleIndex(int index) {
		titleLabel.setText("Segment " + index);
	}
	
	public SegmentUI(final ComplexSignalScreen screen, final Segment segment) {
		super(Lissajous.skin);
		//setBackground("textfieldLight");
		
		this.screen = screen;
		this.segment = segment;
		
		titleLabel = new Label("Segment x", Lissajous.skin, "light");
		titleLabel.setEllipsis(true);
		titleLabel.setAlignment(Align.left);
		Container<Label> titleContainer = new Container<Label>(titleLabel);
		titleContainer.setBackground(Lissajous.skin.getDrawable("textfieldLight"));
		titleContainer.fillX().padLeft(4).minWidth(1);
		add(titleContainer).colspan(1).expandX().fillX().minWidth(1).padBottom(4).padRight(4).left();
		
		SelectBox<Interpolator> intSelect = new SelectBox<Interpolator>(Lissajous.skin, "light");
		intSelect.setItems(Interpolator.values());
		intSelect.setSelected(segment.interpolator);
		intSelect.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				segment.interpolator = intSelect.getSelected();
				screen.getImage().requestReplot();
				screen.getImage().requestRedraw();
			}
		});
		Container<SelectBox<Interpolator>> selectContainer = new Container<SelectBox<Interpolator>>(intSelect);
		selectContainer.setBackground(Lissajous.skin.getDrawable("textfieldLight"));
		selectContainer.fillX().padLeft(4).minWidth(1);
		add(selectContainer).colspan(2).expandX().fillX().minWidth(1).padBottom(4);
		row();
		
		
		fWidth = new NumericTextfield(screen.getImage(), segment.width).setMin(0);
		fWidth.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				double newPeriod = screen.getSignal().calculateTotalWidth();
				screen.getSignal().set("period", newPeriod);
				screen.updatePeriodTextfieldWhenSegmentResized(newPeriod);
			}
		});
		widgets.add(new LineWidget(fWidth, RESIZE_WIDGET_COLOR, Axis.X) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				ComplexSignal signal = screen.getSignal();
				double posX = signal.positionOfSegment(signal.indexOfSegment(segment));
				return worldX - posX;
			}
			@Override
			public void adjustWorldPosition(double x) {
				ComplexSignal signal = screen.getSignal();
				double posX = signal.positionOfSegment(signal.indexOfSegment(segment));
				worldX = x + posX;
			}
		});
		addField("Sirina", fWidth, null);
		
		NumericTextfield fStart = new NumericTextfield(screen.getImage(), segment.start);
		widgets.add(new CircleWidget(fStart, Color.FIREBRICK) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				ComplexSignal signal = screen.getSignal();
				return (worldY-signal.get("y_offset")) / signal.get("amplitude");
			}
			@Override
			public void adjustWorldPosition(double x) {
				ComplexSignal signal = screen.getSignal();
				worldY = segment.start.get() * signal.get("amplitude") + signal.get("y_offset");
				worldX = signal.positionOfSegment(signal.indexOfSegment(segment));
			}
		}.setLockX(true));
		addField("Pocetak", fStart, null);

		NumericTextfield fEnd = new NumericTextfield(screen.getImage(), segment.end);
		widgets.add(new CircleWidget(fEnd, Color.PURPLE) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				ComplexSignal signal = screen.getSignal();
				return (worldY-signal.get("y_offset")) / signal.get("amplitude");
			}
			@Override
			public void adjustWorldPosition(double x) {
				ComplexSignal signal = screen.getSignal();
				worldY = segment.end.get() * signal.get("amplitude") + signal.get("y_offset");
				worldX = signal.positionOfSegment(signal.indexOfSegment(segment)) + segment.width.get();
			}
		}.setLockX(true));
		addField("Kraj", fEnd, null);

		
		TextButton add = new TextButton("DODAJ", Lissajous.skin, "light");
		add.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				int index = screen.getSignal().indexOfSegment(segment);
				/*SegmentUI ui =*/ screen.addSegment(index);
				//ui.getStage().setKeyboardFocus(ui);
			}
		});
		add(add).fillX().minWidth(1).prefWidth(Value.percentWidth(0.5f, titleLabel)).padRight(4);
	
		TextButton delete = new TextButton("UKLONI", Lissajous.skin, "light");
		delete.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(screen.getSignal().getNumOfSegments() > 1) screen.removeSegment(segment);
			}
		});
		add(delete).expandX().fillX().minWidth(1).prefWidth(Value.percentWidth(0.5f, titleLabel));
		
		addListener(new FocusListener() {
			@Override
			public void keyboardFocusChanged (FocusEvent event, Actor actor, boolean focused) {
				if(focused) {
					Actor parent = getParent();
					while(parent != null) {
						if(parent instanceof ScrollPane) {
							ScrollPane pane = (ScrollPane) parent;
							float height = Math.min(pane.getHeight(), getHeight());
							float offset = getHeight() - height;
							pane.scrollTo(getX(), getY()+offset, getWidth(), height);
							break;
						}
						parent = parent.getParent();
					}
					setColor(Color.RED);
				} else {
					setColor(Color.WHITE);
				}
			} 
		});
			
	}
	
	public void addField(String name, final NumericTextfield field, String annotation) {
		screen.registerField(field);
		fields.add(field);
		
		Label label = new Label(name, Lissajous.skin, "light");
		label.setEllipsis("...");
		label.setFillParent(true);
		
		Container<Label> container = new Container<Label>(label);
		container.setBackground(Lissajous.skin.getDrawable("textfieldLight"));
		container.padLeft(3).padRight(4).minWidth(1).left();
		container.addListener(new InputListener() {
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				getStage().setKeyboardFocus(field);
				return true;
			}
		});
		add(container).minWidth(1).fillX().padRight(4).padBottom(4).left();
		
		add(field).minWidth(1).expandX().fillX().padBottom(4).right().colspan(annotation!=null ? 1 : 2);
		
		if(annotation != null) {
			Label ann = new Label(annotation, Lissajous.skin, "light");
			ann.setEllipsis(true);
			ann.setEllipsis(".");
			ann.setFillParent(true);
			Container<Label> annContainer = new Container<Label>(ann);
			annContainer.setBackground(Lissajous.skin.getDrawable("textfieldLight"));
			annContainer.padLeft(3).padRight(4).minWidth(1).left();
			add(annContainer).minWidth(1).padBottom(4);
		}
		
		row();
	}
	
}
