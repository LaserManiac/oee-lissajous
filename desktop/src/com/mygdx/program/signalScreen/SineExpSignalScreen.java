package com.mygdx.program.signalScreen;

import com.badlogic.gdx.graphics.Color;
import com.mygdx.program.resizeWidget.LineWidget;
import com.mygdx.program.resizeWidget.LineWidget.Axis;
import com.mygdx.program.signal.SineExpSignal;

public class SineExpSignalScreen extends SignalScreen<SineExpSignal> {

	private static final Color WIDGET_COLOR = Color.LIME;
	
	public SineExpSignalScreen() {
		
		NumericTextfield fPlot = new NumericTextfield(image, signal.getParameter("plot"));
		new LineWidget(fPlot, Color.FIREBRICK, Axis.X) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				return worldX;
			}
			@Override
			public void adjustWorldPosition(double x) {
				worldX = x;
			}
		};
		addField("Interval", fPlot);
		
		NumericTextfield fAmplitude = new NumericTextfield(image, signal.getParameter("amplitude"));
		new LineWidget(fAmplitude, WIDGET_COLOR, Axis.Y) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				return worldY - signal.get("y_offset");
			}
			@Override
			public void adjustWorldPosition(double x) {
				worldY = x + signal.get("y_offset");
			}
		};
		addField("Amplituda", fAmplitude);
		
		NumericTextfield fPeriod = new NumericTextfield(image, signal.getParameter("period"));
		new LineWidget(fPeriod, WIDGET_COLOR, Axis.X) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				return worldX;
			}
			@Override
			public void adjustWorldPosition(double x) {
				worldX = x;
			}
		};
		addField("Period", fPeriod);
		
		NumericTextfield fPhase = new NumericTextfield(image, signal.getParameter("phase"));
		new LineWidget(fPhase, Color.RED, Axis.X) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				return -worldX/signal.get("period")*360;
			}
			@Override
			public void adjustWorldPosition(double x) {
				worldX = -x/360*signal.get("period");
			}
		};
		addField("Faza", fPhase, "deg");
		
		NumericTextfield fExp = new NumericTextfield(image, signal.getParameter("exp"));
		addField("Eksponent", fExp);
		
		NumericTextfield fExpShift = new NumericTextfield(image, signal.getParameter("exp_shift"));
		new LineWidget(fExpShift, Color.FIREBRICK, Axis.X) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				return worldX;
			}
			@Override
			public void adjustWorldPosition(double x) {
				worldX = x;
			}
		};
		addField("Eksp. pomak", fExpShift);
		
	}
	
	@Override
	protected SineExpSignal createSignal() {
		return new SineExpSignal();
	}

}
