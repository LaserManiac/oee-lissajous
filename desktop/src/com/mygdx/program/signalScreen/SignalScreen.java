package com.mygdx.program.signalScreen;

import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.StreamUtils;
import com.mygdx.program.Lissajous;
import com.mygdx.program.graphImage.GraphImage.GridRenderMode;
import com.mygdx.program.graphImage.SignalImage;
import com.mygdx.program.resizeWidget.CircleWidget;
import com.mygdx.program.resizeWidget.LineWidget;
import com.mygdx.program.resizeWidget.LineWidget.Axis;
import com.mygdx.program.resizeWidget.ResizeWidget;
import com.mygdx.program.saving.FileChooser;
import com.mygdx.program.signal.Parameter;
import com.mygdx.program.signal.Signal;
import com.mygdx.program.signalScreen.complex.ComplexSignalScreen;
import com.mygdx.program.signalScreen.selectBox.SignalScreenSelectBox;
import com.mygdx.program.signalScreen.selectBox.SignalScreenType;

public abstract class SignalScreen<T extends Signal> extends Table implements Disposable {
	
	private final Color WIDGET_COLOR = Color.ORANGE.cpy().mul(0.8f, 0.8f, 0.8f, 1.0f);
	
	private SignalScreenType type;
	public void setType(SignalScreenType type) {
		this.type = type;
		setSelected(type);
	}
	
	protected T signal;
	public T getSignal() {
		return signal;
	}

	private final SignalScreenSelectBox signalSelectBox;
	public void setSelected(SignalScreenType item) {
		signalSelectBox.getSelection().setProgrammaticChangeEvents(false);
		signalSelectBox.setSelected(item);
		signalSelectBox.getSelection().setProgrammaticChangeEvents(true);
	}
	
	protected SignalImage image;
	protected Table fieldTable, titleTable;
	
	protected List<NumericTextfield> fields = new ArrayList<NumericTextfield>();
	public List<NumericTextfield> getFields() {
		return fields;
	}
	public void updateFields() {
		for(NumericTextfield field : fields) {
			field.updateFromParam();
		}
	}
	public void registerField(NumericTextfield field) {
		fields.add(field);
	}
	public void unregisterField(NumericTextfield field) {
		fields.remove(field);
	}
	
	public SignalScreen() {
		super();
		
		image = new SignalImage();
		
		fieldTable = new Table(Lissajous.skin).top().left().padRight(4);
		
		titleTable = new Table(Lissajous.skin);
		
		Table titleSub1 = new Table(Lissajous.skin);
		titleSub1.setBackground("textfieldLight");
		
		signalSelectBox = new SignalScreenSelectBox(this);
		titleSub1.add(signalSelectBox).minWidth(1).expandX().left().padLeft(4).padRight(4);
		
		ImageButton save = new ImageButton(Lissajous.skin, "save");
		save.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				getStage().addActor(new FileChooser(true, ".lissajous/saves/", ".signal", (file) -> save(file)));
			}
		});
		titleSub1.add(save).padRight(5);
		
		ImageButton load = new ImageButton(Lissajous.skin, "load");
		load.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				getStage().addActor(new FileChooser(false, ".lissajous/saves/", ".signal", (file) -> load(file)));
			}
		});
		titleSub1.add(load).padRight(4);
		
		ImageButton camera = new ImageButton(Lissajous.skin, "camera");
		camera.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
				String name = "signal " + (Lissajous.signal1==SignalScreen.this ? "x" : "y") + " " + format.format(new Date());
				FileHandle file = image.snapshot(name);
				getStage().addActor(new ImageSavedMessage("SLIKA SIGNALA SPREMLJENA!", file));
			}
		});
		titleSub1.add(camera).padRight(4);
		
		ImageButton grid = new ImageButton(Lissajous.skin, "grid_0");
		grid.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				GridRenderMode mode = GridRenderMode.values()[(image.getGridRenderMode().ordinal()+1)%3];
				image.setGridRenderMode(mode);
				image.requestRedraw();
				grid.setStyle(Lissajous.skin.get("grid_" + mode.ordinal(), ImageButtonStyle.class));
			}
		});
		titleSub1.add(grid).padRight(4);
		
		ImageButton hide = new ImageButton(Lissajous.skin, "toggleWidgets");
		hide.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				ImageButton button = (ImageButton) actor;
				if(button.isChecked()) {
					for(Actor child : image.getChildren()) {
						if(child instanceof ResizeWidget) {
							child.setTouchable(Touchable.disabled);
							child.setVisible(false);
						}
					}
				} else {
					for(Actor child : image.getChildren()) {
						if(child instanceof ResizeWidget) {
							child.setTouchable(Touchable.enabled);
							child.setVisible(true);
						}
					}
				}
			}
		});
		titleSub1.add(hide).right().padRight(4);
		
		titleTable.add(titleSub1).expandX().fillX();
		
		final ScrollPane pane = new ScrollPane(fieldTable, Lissajous.skin, "plain");
		pane.setupFadeScrollBars(0.1f, 1.0f);
		pane.setScrollingDisabled(true, false);
		pane.setFadeScrollBars(false);
		pane.setFlickScroll(false);
		pane.setOverscroll(false, false);
		pane.addListener(new InputListener(){
			@Override
			public void enter (InputEvent event, float x, float y, int pointer, Actor fromActor) {
				getStage().setScrollFocus(pane);
			}
		});
		
		Table contentTable = new Table(Lissajous.skin).top().left().pad(4);
		contentTable.setClip(true);
		contentTable.setBackground("white");
		contentTable.setColor(Lissajous.skin.getColor("bcg-light"));
		contentTable.add(titleTable).expandX().fillX().padBottom(4);
		contentTable.row();
		contentTable.add(pane).expandX().fillX();
		
		Container<Table> container = new Container<Table>(contentTable);
		container.setBackground(Lissajous.skin.getDrawable("border"));
		container.fill();
		
		add(container).expandY().fill().width(Value.percentWidth(0.3f, this)).padRight(8);
		add(image).expandY().fill().prefWidth(Value.percentWidth(0.7f, this));
		
		addListener(new InputListener() {
			@Override
			public void enter (InputEvent event, float x, float y, int pointer, Actor fromActor) {
				toFront();
			}
		});
		
		signal = createSignal();
		image.setSignal(signal);
		
		NumericTextfield fYOff = new NumericTextfield(image, signal.getParameter("y_offset"));
		new CircleWidget(fYOff, WIDGET_COLOR) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				return worldY;
			}
			@Override
			public void adjustWorldPosition(double x) {
				worldY = x;
			}
		}.setLockX(true);
		addField("Y pomak", fYOff);
		
		NumericTextfield fMax = new NumericTextfield(image, signal.getParameter("max"));
		new LineWidget(fMax, WIDGET_COLOR, Axis.Y) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				return worldY - signal.get("y_offset");
			}
			@Override
			public void adjustWorldPosition(double x) {
				worldY = x + signal.get("y_offset");
			}
		};
		addField("Max", fMax);
		
		NumericTextfield fMin = new NumericTextfield(image, signal.getParameter("min"));
		new LineWidget(fMin, WIDGET_COLOR, Axis.Y) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				return worldY - signal.get("y_offset");
			}
			@Override
			public void adjustWorldPosition(double x) {
				worldY = x + signal.get("y_offset");
			}
		};
		addField("Min", fMin);
	}
	
	public void addField(String name, final NumericTextfield field) {
		addField(name, field, null);
	}
	
	public void addField(String name, final NumericTextfield field, String annotation) {
		registerField(field);
		
		Label label = new Label(name, Lissajous.skin, "light");
		label.setEllipsis("...");
		label.setFillParent(true);
		
		Container<Label> container = new Container<Label>(label);
		container.setBackground(Lissajous.skin.getDrawable("textfieldLight"));
		container.padLeft(3).padRight(4).minWidth(1).left();
		container.addListener(new InputListener() {
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				getStage().setKeyboardFocus(field);
				return true;
			}
		});
		fieldTable.add(container).minWidth(1).fillX().padRight(4).padBottom(4).left();
		
		fieldTable.add(field).minWidth(1).expandX().fillX().padBottom(4).right().colspan(annotation!=null ? 1 : 2);
		
		if(annotation != null) {
			Label ann = new Label(annotation, Lissajous.skin, "light");
			ann.setEllipsis(true);
			ann.setEllipsis(".");
			ann.setFillParent(true);
			Container<Label> annContainer = new Container<Label>(ann);
			annContainer.setBackground(Lissajous.skin.getDrawable("textfieldLight"));
			annContainer.padLeft(3).padRight(4).minWidth(1).left();
			fieldTable.add(annContainer).minWidth(1).padBottom(4);
		}
		
		fieldTable.row();
	}
	
	protected abstract T createSignal();
	
	public void toJson(Json json) {
		json.writeValue("type", type.identifier);

		for(Entry<String, Parameter> entry : signal.getParameters().entrySet()) {
			json.writeValue(entry.getKey(), entry.getValue().get());
		}
	}
	
	public void fromJson(JsonValue json) {
		String identifier = json.getString("type");
		
		try {
			
			SignalScreenType type = SignalScreenType.getTypeFromIdentifier(identifier);
			if(type == null) throw new RuntimeException("Unknown function type: " + identifier);
			SignalScreen<?> screen = type.createSignalScreen();
			
			for(Entry<String, Parameter> entry : screen.getSignal().getParameters().entrySet()) {
				double val = json.getDouble(entry.getKey());
				entry.getValue().set(val);
			}
			
			if(screen instanceof ComplexSignalScreen) {
				ComplexSignalScreen css = (ComplexSignalScreen) screen;
				boolean periodic = json.getBoolean("periodic");
				css.setPeriodic(periodic);
				ComplexSignalScreen.loadSegments(css, json.get("segments"));
			}
			
			screen.updateFields();
			
			if(Lissajous.signal1 == this)
				Lissajous.setSignalScreen1(screen);
			else if(Lissajous.signal2 == this)
				Lissajous.setSignalScreen2(screen);
			
		} catch(Exception e) {
			System.out.println("ERORR loading signal: " + e.getMessage());
		}
	}
	
	public void save(FileHandle file) {
		
		Writer writer = file.writer(false);
		
		try {
			
			Json json = new Json();
			json.setWriter(writer);
			json.writeObjectStart();
			toJson(json);
			json.writeObjectEnd();
			
		} catch(Exception e) {
			System.out.println("ERROR saving signal: " + e.getMessage());
		} finally {
			StreamUtils.closeQuietly(writer);
		}
	}
	
	public void load(FileHandle file) {
		try {
			
			JsonReader reader = new JsonReader();
			JsonValue json = reader.parse(file);
			fromJson(json);
			
		} catch(Exception e) {
			System.out.println("ERROR loading signal: " + e.getMessage());
		}
	}
	
	@Override
	public void dispose() {
		image.dispose();
	}
	
}
