package com.mygdx.program.signalScreen;

import com.badlogic.gdx.graphics.Color;
import com.mygdx.program.resizeWidget.CircleWidget;
import com.mygdx.program.signal.SawtoothSignal;

public class SawtoothSignalScreen extends PeriodicSignalScreen<SawtoothSignal> {
	
	public SawtoothSignalScreen() {
		NumericTextfield fTooth = new NumericTextfield(image, signal.getParameter("tooth")).setMin(0.0).setMax(1.0);
		new CircleWidget(fTooth, Color.RED) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				return (worldX + signal.get("phase")/360*signal.get("period")) / signal.get("period");
			}
			@Override
			public void adjustWorldPosition(double x) {
				worldY = signal.get("amplitude") + signal.get("y_offset");
				worldX = (x - signal.get("phase")/360) * signal.get("period");
			}
		}.setLockY(true);
		addField("Pomak vrha", fTooth);
	}
	
	@Override
	protected SawtoothSignal createSignal() {
		return new SawtoothSignal();
	}
	
}
