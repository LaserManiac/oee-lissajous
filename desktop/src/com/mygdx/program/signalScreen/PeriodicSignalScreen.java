package com.mygdx.program.signalScreen;

import com.badlogic.gdx.graphics.Color;
import com.mygdx.program.resizeWidget.LineWidget;
import com.mygdx.program.resizeWidget.LineWidget.Axis;
import com.mygdx.program.signal.PeriodicSignal;

public abstract class PeriodicSignalScreen<T extends PeriodicSignal> extends SignalScreen<T> {
	
	private static final Color WIDGET_COLOR = Color.LIME;
	
	public PeriodicSignalScreen() {
		
		NumericTextfield fAmplitude = new NumericTextfield(image, signal.getParameter("amplitude"));
		new LineWidget(fAmplitude, WIDGET_COLOR, Axis.Y) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				return worldY - signal.get("y_offset");
			}
			@Override
			public void adjustWorldPosition(double x) {
				worldY = x + signal.get("y_offset");
			}
		};
		addField("Amplituda", fAmplitude);
		
		NumericTextfield fPeriod = new NumericTextfield(image, signal.getParameter("period"));
		new LineWidget(fPeriod, WIDGET_COLOR, Axis.X) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				return worldX;
			}
			@Override
			public void adjustWorldPosition(double x) {
				worldX = x;
			}
		};
		addField("Period", fPeriod);
		
		NumericTextfield fPhase = new NumericTextfield(image, signal.getParameter("phase"));
		new LineWidget(fPhase, Color.RED, Axis.X) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				return -worldX/signal.get("period")*360;
			}
			@Override
			public void adjustWorldPosition(double x) {
				worldX = -x/360*signal.get("period");
			}
		};
		addField("Faza", fPhase, "deg");

	}
	
}
