package com.mygdx.program.signalScreen.selectBox;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.mygdx.program.Lissajous;
import com.mygdx.program.signalScreen.SawtoothSignalScreen;
import com.mygdx.program.signalScreen.SignalScreen;
import com.mygdx.program.signalScreen.SineExpSignalScreen;
import com.mygdx.program.signalScreen.SineSignalScreen;
import com.mygdx.program.signalScreen.SquareSignalScreen;
import com.mygdx.program.signalScreen.complex.ComplexSignalScreen;
import com.mygdx.program.signalScreen.expression.ExpressionSignalScreenBlueprint;
import com.mygdx.program.signalScreen.expression.ExpressionSignalScreenBlueprintLoader;

public abstract class SignalScreenType {
	
	public static final List<SignalScreenType> types = new ArrayList<>();
	
	public static SignalScreenType getTypeFromIdentifier(String identifier) {
		for(SignalScreenType type : types) {
			if(type.identifier.equals(identifier)) {
				return type;
			}
		}
		return null;
	}
	
	public static void loadTypes() {
		try {
			
			types.add(new ClassScreenType("cls_sin", "Sinusni", SineSignalScreen.class));
			types.add(new ClassScreenType("cls_tri", "Trokutasti", SawtoothSignalScreen.class));
			types.add(new ClassScreenType("cls_sqr", "Pravokutni", SquareSignalScreen.class));
			types.add(new ClassScreenType("cls_sxp", "Sinus s eksp.", SineExpSignalScreen.class));
			types.add(new ClassScreenType("cls_cmp", "Kompleksni", ComplexSignalScreen.class));
			
			FileHandle logFile = Gdx.files.external(".lissajous/custom/log.txt");
			
			String header = "Pokretanje " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "\n";
			logFile.writeString(header, false);
			int errors = 0;
			
			FileHandle folder = Gdx.files.external(".lissajous/custom");
			if(folder.exists()) {
				FileHandle[] list = folder.list((f) -> f.isFile() && f.getName().endsWith(".sbl"));
				if(list.length > 0) {
					ExpressionSignalScreenBlueprintLoader loader = new ExpressionSignalScreenBlueprintLoader();
					for(FileHandle file : list) {
						
						try {
							
							ExpressionSignalScreenBlueprint blueprint = loader.load(file);
							types.add(blueprint);
							
						} catch(Exception e) {
							
							String errorMsg = "Pogreska kod ucitavanja SBL datoteke \"" + file.name() + "\": " + e.getMessage();
							logFile.writeString(errorMsg + "\n", true);
							errors++;
							System.out.println(errorMsg);
							
						}
					}
				}
			}
			
			if(errors == 0) {
				logFile.writeString("Nema gresaka!", true);
			}
			
		} catch(Exception e) {
			//if(Lissajous.debug)
				e.printStackTrace();
		}
	}
	
	
	
	public final String identifier;
	public final String name;
	
	public SignalScreenType(String identifier, String name) {
		this.identifier = identifier;
		this.name = name;
	}
	
	public abstract SignalScreen<?> createSignalScreen();
	
	@Override
	public String toString() {
		return name;
	}
	
	
	
	private static class ClassScreenType extends SignalScreenType {

		private final Class<? extends SignalScreen<?>> cls;
		
		public ClassScreenType(String identifier, String name, Class<? extends SignalScreen<?>> cls) {
			super(identifier, name);
			this.cls = cls;
		}
		
		@Override
		public SignalScreen<?> createSignalScreen() {
			try {
				SignalScreen<?> screen = cls.newInstance();
				screen.setType(this);
				return screen;
			} catch(Exception e) {
				if(Lissajous.debug) e.printStackTrace();
				return null;
			}
		}
		
	}
	
}
