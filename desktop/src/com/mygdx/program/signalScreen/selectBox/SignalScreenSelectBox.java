package com.mygdx.program.signalScreen.selectBox;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.mygdx.program.Lissajous;
import com.mygdx.program.signalScreen.SignalScreen;

public class SignalScreenSelectBox extends SelectBox<SignalScreenType> {

	public SignalScreenSelectBox(SignalScreen<?> parentScreen) {
		super(Lissajous.skin, "light");
		
		SignalScreenType[] items = new SignalScreenType[SignalScreenType.types.size()];
		SignalScreenType.types.toArray(items);
		setItems(items);
		
		addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				SignalScreenType selected = getSelected();
				
				try{
					SignalScreen<?> newScreen = selected.createSignalScreen();
					if(Lissajous.signal1 == parentScreen)
						Lissajous.setSignalScreen1(newScreen);
					else if(Lissajous.signal2 == parentScreen)
						Lissajous.setSignalScreen2(newScreen);
				} catch(Exception e) {
					e.printStackTrace();
				}
				
			}
		});
	}
	
}
