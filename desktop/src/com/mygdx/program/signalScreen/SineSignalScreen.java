package com.mygdx.program.signalScreen;

import com.mygdx.program.signal.SineSignal;

public class SineSignalScreen extends PeriodicSignalScreen<SineSignal> {
	
	@Override
	protected SineSignal createSignal() {
		return new SineSignal();
	}
}
