package com.mygdx.program.signalScreen.expression;

public final class ParameterBlueprint {

	public final String name;
	public final double initial, min, max;
	
	public ParameterBlueprint(String name, double initial, double min, double max) {
		this.name = name;
		this.initial = initial;
		this.min = min;
		this.max = max;
	}
	
}
