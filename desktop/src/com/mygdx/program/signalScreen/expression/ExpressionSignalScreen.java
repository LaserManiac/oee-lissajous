package com.mygdx.program.signalScreen.expression;

import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.mygdx.program.resizeWidget.LineWidget;
import com.mygdx.program.resizeWidget.LineWidget.Axis;
import com.mygdx.program.signal.ExpressionSignal;
import com.mygdx.program.signalScreen.NumericTextfield;
import com.mygdx.program.signalScreen.SignalScreen;

public abstract class ExpressionSignalScreen extends SignalScreen<ExpressionSignal> {

	public ExpressionSignalScreen(List<ParameterBlueprint> parameters) {
		
		if(!signal.isPeriodic()) {
			NumericTextfield field = new NumericTextfield(image, signal.getParameter("plot"));
			new LineWidget(field, Color.FIREBRICK, Axis.X) {
				@Override
				public double worldToFunctionValue(double worldX, double worldY) {
					return worldX;
				}
				@Override
				public void adjustWorldPosition(double x) {
					worldX = x;
				}
			};
			addField("Interval", field);
		}
		
		for(ParameterBlueprint parameter : parameters) {
			NumericTextfield field = new NumericTextfield(image, signal.getParameter(parameter.name));
			field.setMax(parameter.max).setMin(parameter.min);
			addField(parameter.name, field);
		}

	}
	
	@Override
	protected abstract ExpressionSignal createSignal();

}
