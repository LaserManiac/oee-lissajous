package com.mygdx.program.signalScreen.expression;

import java.util.List;

import com.mygdx.program.expression.Expression;
import com.mygdx.program.expression.Variable;
import com.mygdx.program.signal.ExpressionSignal;
import com.mygdx.program.signalScreen.SignalScreen;
import com.mygdx.program.signalScreen.selectBox.SignalScreenType;

public final class ExpressionSignalScreenBlueprint extends SignalScreenType {

	public final Expression expression;
	public final Expression period;
	public final List<ParameterBlueprint> parameters;
	public final List<Variable> variables;
	
	public ExpressionSignalScreenBlueprint(String identifier, String name, Expression expression, Expression period, List<ParameterBlueprint> parameters, List<Variable> variables) {
		super(identifier, name);
		this.expression = expression;
		this.period = period;
		this.parameters = parameters;
		this.variables = variables;
	}

	@Override
	public SignalScreen<?> createSignalScreen() {
		final ExpressionSignal _signal = new ExpressionSignal(expression, period, parameters, variables);
		ExpressionSignalScreen screen = new ExpressionSignalScreen(parameters) {
			@Override
			protected ExpressionSignal createSignal() {
				return _signal;
			}
		};
		screen.setType(this);
		return screen;
	}
	
}
