package com.mygdx.program.signalScreen.expression;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.badlogic.gdx.files.FileHandle;
import com.mygdx.program.expression.Expression;
import com.mygdx.program.expression.ExpressionParser;
import com.mygdx.program.expression.ExpressionParser.ParseMode;
import com.mygdx.program.expression.Variable;
import com.mygdx.program.expression.map.ConstantMap;
import com.mygdx.program.expression.map.MacroMap;
import com.mygdx.program.expression.map.ParameterLinkMap;
import com.mygdx.program.expression.map.VariableMap;

public final class ExpressionSignalScreenBlueprintLoader {

	private static final Pattern pat_Comment = Pattern.compile("(?ms)^[ \\t]*#.*?\\n");
	private static final Pattern pat_Statement = Pattern.compile("(?ms)^\\S.*?(?=;)");
	private static final Pattern pat_Split = Pattern.compile("\"([^\"]*)\"|\\[([^\\]]*)\\] *|([^ ]+) *");
	private static final Pattern pat_Name = Pattern.compile("[A-Za-z0-9]+[A-Za-z0-9 ]*");
	private static final Pattern pat_TokenName = Pattern.compile("[A-Za-z]+[A-Za-z0-9]*");
	
	public ExpressionSignalScreenBlueprint load(FileHandle handle) {
		String data = handle.readString();
		
		//First remove any comments
		Matcher commentMatcher = pat_Comment.matcher(data);
		while(commentMatcher.find()) {
			data = data.substring(0, commentMatcher.start()) + data.substring(commentMatcher.end());
			commentMatcher.reset(data);
		}
		//System.out.println("\nWithout any comments: \n" + data + "\n==== ==== ==== ==== ==== ==== ==== ====");
		
		// Now parse into statement list
		List<String> statements = new ArrayList<>();
		Matcher lineMatcher = pat_Statement.matcher(data);
		while(lineMatcher.find()) {
			String statement = lineMatcher.group();
			statement = statement.replaceAll("\n", "").replaceAll("\r", "").replaceAll("\t", "");
			statements.add(statement);
			//System.out.println("-> " + statement);
		}
		
		// Just some data
		String signalName = null;
		Expression expression = null;
		Expression period = null;
		ConstantMap constants = new ConstantMap();
		VariableMap variables = new VariableMap();
		MacroMap macros = new MacroMap();
		ParameterLinkMap parameterLinks = new ParameterLinkMap();
		List<ParameterBlueprint> parameterBlueprints = new ArrayList<>();
		ExpressionParser xprParser = new ExpressionParser();
		//xprParser.setDebug(true);
		
		// Now parse statement by statement
		int statementIndex = 1;
		for(String statement : statements) {
			if(statement.isEmpty()) {
				// Nothing kek
			} else if(statement.startsWith("#")) {
				//System.out.println(lineIndex + ": Comment: \"" + line.substring(1) + "\"");
			} else {
				List<String> elements = new ArrayList<>();
				Matcher matcher = pat_Split.matcher(statement);
				while(matcher.find()) {
					if(matcher.group(1) != null) elements.add(matcher.group(1));
					else if(matcher.group(2) != null) elements.add(matcher.group(2));
					else if(matcher.group(3) != null) elements.add(matcher.group(3));
				}
				
				String command = elements.get(0);
				if(command.equals("SBL")) {
					checkNumberOfElements(elements, 2, statementIndex);
					if(signalName!= null) error(statementIndex, "Name redefinition!");
					
					if(statementIndex != 1)
						error(statementIndex, "Deklaracija imena mora uvijek biti prva u datoteci!");
					signalName = elements.get(1);
					if(!pat_Name.matcher(signalName).matches())
						error(statementIndex, "Ime moze sadrzavati samo slova, brojeve, i razmake, te ne smije pocimati razmakom!");
					
				} else if(command.equals("const")) {
					checkNumberOfElements(elements, 3, statementIndex);
					
					String name = elements.get(1);
					if(!pat_TokenName.matcher(name).matches())
						error(statementIndex, "Ime konstante smije sadrzavati samo slova i brojeve, te mora pocimati slovom!");
					
					if(constants.containsKey(name)) error(statementIndex, "Konstanta \"" + name + "\" vec postoji!");
					
					double value = parseNumber(elements.get(2), statementIndex);
					
					constants.add(name, value);
					
				} else if(command.equals("param")) {
					checkNumberOfElements(elements, 4, statementIndex);
					
					String name = elements.get(1);
					if(!pat_TokenName.matcher(name).matches())
						error(statementIndex, "Ime parametra smije sadrzavati samo slova i brojeve, te mora pocimati slovom!");
					
					if(name.equals("y_offset") || name.equals("min") || name.equals("max"))
						error(statementIndex, "Parametar \"" + name + "\" je ugradjen parametar i nije ga moguce redefinirati!");
					
					if(parameterLinks.containsKey(name)) error(statementIndex, "Parametar \"" + name + "\" vec postoji!");
					
					double init = parseNumber(elements.get(2), statementIndex);
					
					String[] minMax = elements.get(3).split(",");
					if(minMax.length != 2) error(statementIndex, "Krivi broj vrijednosti u zagradama!");
					double min = parseNumber(minMax[0].trim(), statementIndex);
					double max = parseNumber(minMax[1].trim(), statementIndex);
					
					parameterLinks.add(name);
					parameterBlueprints.add(new ParameterBlueprint(name, init, min, max));
					
				} else if(command.equals("var")) {
					checkNumberOfElements(elements, 3, statementIndex);
				
					String name = elements.get(1);
					if(!pat_TokenName.matcher(name).matches())
						error(statementIndex, "Ime varijable smije sadrzavati samo slova i brojeve, te mora pocimati slovom!");
						
					if(variables.containsKey(name)) error(statementIndex, "Varijabla \"" + name + "\" vec postoji!");
					
					String xpr = elements.get(2);
					try {
						Expression varXpr = xprParser.parse(xpr, constants, variables, macros, parameterLinks, ParseMode.STATIC);
						variables.add(name, varXpr);
					} catch(Exception e) {
						error(statementIndex, "Problem kod ucitavanja izraza varijable: " + e.getMessage());
					}
					
				} else if(command.equals("macro")) {
					checkNumberOfElements(elements, 4, statementIndex);
				
					String name = elements.get(1);
					if(!pat_TokenName.matcher(name).matches())
						error(statementIndex, "Ime makro funkcije smije sadrzavati samo slova i brojeve, te mora pocimati slovom!");
					
					if(macros.containsKey(name)) error(statementIndex, "Makro funkcija \"" + name + "\" vec postoji!");
					
					String[] args = elements.get(2).split(",");
					if(elements.get(2).isEmpty()) args = null;
					
					if(args != null) {
						for(int i = 0; i < args.length; i++) {
							args[i] = args[i].trim();
							if(!pat_TokenName.matcher(args[i]).matches())
								error(statementIndex, "Argumenti makro funkcije smiju sadrzavati samo slova i brojeve, te moraju pocimati slovom!");
						}
					}
					String xpr = elements.get(3);
					
					try {
						macros.add(name, xpr, args);
					} catch(Exception e) {
						error(statementIndex, e.getMessage());
					}
					
				} else if(command.equals("period")) {
					checkNumberOfElements(elements, 2, statementIndex);
					if(period != null) error(statementIndex, "Period je vec definiran!");
					
					String xpr = elements.get(1);
					try {
						period = xprParser.parse(xpr, constants, variables, macros, parameterLinks, ParseMode.STATIC);
					} catch(Exception e) {
						error(statementIndex, "Problem kod ucitavanja izraza perioda: " + e.getMessage());
					}
					
				} else if(command.equals("def")) {
					checkNumberOfElements(elements, 2, statementIndex);
					if(expression != null) error(statementIndex, "Signal je vec definiran!");
					
					String xpr = elements.get(1);
					try {
						expression = xprParser.parse(xpr, constants, variables, macros, parameterLinks, ParseMode.DYNAMIC);
					} catch(Exception e) {
						error(statementIndex, "Problem kod ucitavanja izraza signala: " + e.getMessage());
					}
					
				} else {
					error(statementIndex, "Nepoznata naredba: " + command);
				}
				
			}
			statementIndex++;
		}
		
		// Check if any statements are missing
		if(signalName == null) error(statementIndex, "Nedostaje deklaracija imena!");
		if(expression == null) error(statementIndex, "Nedostaje definicija signala!");
		
		// Build variable list
		List<Variable> variableList = new ArrayList<>();
		variables.values().forEach((var) -> variableList.add(var));
		
		// Create blueprint and return
		ExpressionSignalScreenBlueprint blueprint = new ExpressionSignalScreenBlueprint(handle.name(), signalName, expression, period, parameterBlueprints, variableList);
		return blueprint;
	}
	
	private void checkNumberOfElements(List<?> list, int elements, int line) {
		if(list.size() != elements) {
			error(line, "Pogresan broj elemenata naredbe!");
		}
	}
	
	private double parseNumber(String str, int statement) {
		if(str.equals("inf")) return Integer.MAX_VALUE;
		else if(str.equals("-inf")) return Integer.MIN_VALUE;
		else try {
			return Double.parseDouble(str);
		} catch(Exception e) {
			error(statement, str + " nije vrijedeci broj!");
		}
		return 0;
	}
	
	private static void error(int statement, String error) {
		throw new RuntimeException("[Naredba " + statement + "] " + error);
	}
	
}
