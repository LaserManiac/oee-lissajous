package com.mygdx.program.signalScreen;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import com.mygdx.program.Lissajous;
import com.mygdx.program.evaluator.Evaluator;
import com.mygdx.program.graphImage.GraphImage;
import com.mygdx.program.resizeWidget.ResizeWidget;
import com.mygdx.program.signal.Parameter;

public class NumericTextfield extends TextField {

	protected double value;
	public double getValue() {
		return value;
	}
	/** For the loopback hack, pls don't use for anything else */
	public void setValue(double value) {
		this.value = value;
	}
	public boolean updateValue() {
		try {
			double v = Evaluator.parse(getText());
			value = v;
			
			if(value < min) {
				value = min;
				setProgrammaticChangeEvents(false);
				setText(Lissajous.doubleToString(value));
				setProgrammaticChangeEvents(true);
			}
			
			if(value > max) {
				value = max;
				setProgrammaticChangeEvents(false);
				setText(Lissajous.doubleToString(value));
				setProgrammaticChangeEvents(true);
			}
			
			setColor(Color.WHITE);
			return true;
		} catch(Exception e) {
			setColor(0.9f, 0.5f, 0.5f, 1.0f);
			if(Lissajous.debug) System.out.println("ERROR: " + e.getMessage());
			return false;
		}
	}
	
	protected double min = Integer.MIN_VALUE;
	public NumericTextfield setMin(double min) {
		this.min = min;
		return this;
	}
	
	protected double max = Integer.MAX_VALUE;
	public NumericTextfield setMax(double max) {
		this.max = max;
		return this;
	}
	
	protected final GraphImage image;
	protected final Parameter parameter;
	public void updateFromParam() {
		setText(Lissajous.doubleToString(parameter.get()));
	}
	
	protected ResizeWidget widget;
	public void setWidget(ResizeWidget widget) {
		this.widget = widget;
		widget.adjustWorldPosition(value);
		image.addActor(widget);
	}
	
	public NumericTextfield(GraphImage image, Parameter parameter) {
		super("0.0", Lissajous.skin, "light");
		this.image = image;
		this.parameter = parameter;
		this.value = parameter.get();
		setText(Lissajous.doubleToString(value));
		
		setProgrammaticChangeEvents(true);
		
		addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(updateValue()) updateData();
			}
		});
		
		addListener(new FocusListener() {
			@Override
			public void keyboardFocusChanged (FocusEvent event, Actor actor, boolean focused) {
				if(focused) {
					Actor parent = getParent();
					while(parent != null) {
						if(parent instanceof ScrollPane) {
							ScrollPane pane = (ScrollPane) parent;
							pane.scrollTo(getX(), getY(), getWidth(), getHeight());
							break;
						}
						parent = parent.getParent();
					}
				} else {
					updateFromParam();
				}
			} 
		});
		
		addListener(new InputListener() {
			@Override
			public boolean keyDown (InputEvent event, int keycode) {
				if(keycode == Keys.ENTER) {
					getStage().unfocus(NumericTextfield.this);
					updateFromParam();
					return true;
				}
				if(keycode == Keys.UP) {
					value = (value*10 + 1)/10;
					setText(Lissajous.doubleToString(value));
					return true;
				}
				if(keycode == Keys.DOWN) {
					value = (value*10 - 1)/10;
					setText(Lissajous.doubleToString(value));
					return true;
				}
				return false;
			}
		});
	}
	
	public void updateData() {
		parameter.set(value);
		if(widget != null) widget.adjustWorldPosition(value);
		image.requestReplot();
		image.requestRedraw();
	}
	
	/*
	public static int i = 0;
	@Override
	public void setText(String text) {
		super.setText(text);
		
		System.out.println("================ Test " + (i++) + " for " + ((Object)this).hashCode() + " ================");
		StackTraceElement[] trace = new Throwable().getStackTrace();
		for(StackTraceElement element : trace) {
			System.out.println("-> " + element.toString());
		}
		System.out.println("================ End ================");
	}
	*/

}
