package com.mygdx.program.signalScreen;

import com.badlogic.gdx.graphics.Color;
import com.mygdx.program.resizeWidget.LineWidget;
import com.mygdx.program.resizeWidget.LineWidget.Axis;
import com.mygdx.program.signal.SquareSignal;

public class SquareSignalScreen extends PeriodicSignalScreen<SquareSignal> {
	
	public SquareSignalScreen() {
		NumericTextfield fSplit = new NumericTextfield(image, signal.getParameter("split")).setMin(0.0).setMax(1.0);
		new LineWidget(fSplit, Color.RED, Axis.X) {
			@Override
			public double worldToFunctionValue(double worldX, double worldY) {
				return (worldX + signal.get("phase")/360*signal.get("period")) / signal.get("period");
			}
			@Override
			public void adjustWorldPosition(double x) {
				worldX = (x - signal.get("phase")/360) * signal.get("period");
			}
		};
		addField("Sredina", fSplit);
	}
	
	@Override
	protected SquareSignal createSignal() {
		return new SquareSignal();
	}
	
}
