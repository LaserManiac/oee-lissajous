package com.mygdx.program.desktop;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.mygdx.program.Lissajous;

public class DesktopLauncher {
	public static void main (String[] arg) {
		
		/*
		MacroBlueprint bp = new MacroBlueprint("sin(%hello%world*3)", "hello", "world");
		bp.setArgument(0, "3.1415");
		bp.setArgument(1, "$Test + 1337.0");
		System.out.println(bp);
		
		if(true != (true && false)) return;
		/**/
		
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		ShaderProgram.prependVertexCode = "#version 330" + System.getProperty("line.separator");
		ShaderProgram.prependFragmentCode = "#version 330" + System.getProperty("line.separator");
		
		config.resizable = true;
		config.width = 700;
		config.height = 550;
		config.title = "Lissajous";
		config.foregroundFPS = 0;
		config.vSyncEnabled = false;
		
		config.addIcon("icon/icon_96.png", FileType.Internal);
		config.addIcon("icon/icon_64.png", FileType.Internal);
		config.addIcon("icon/icon_48.png", FileType.Internal);
		config.addIcon("icon/icon_32.png", FileType.Internal);
		config.addIcon("icon/icon_24.png", FileType.Internal);
		config.addIcon("icon/icon_16.png", FileType.Internal);
		
		new LwjglApplication(new Lissajous(), config);
	}
}
