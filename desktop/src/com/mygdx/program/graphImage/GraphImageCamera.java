package com.mygdx.program.graphImage;

import com.badlogic.gdx.graphics.OrthographicCamera;

public class GraphImageCamera extends OrthographicCamera {

	public double x, y, dZoom;
	
	public GraphImageCamera() {
		dZoom = 0.001f;
		zoom = (float) dZoom;
	}
	
	public void setViewportSize(float width, float height) {
		viewportWidth = width;
		viewportHeight = height;
	}
	
	public float getWorldWidth() {
		return (float)(viewportWidth*dZoom);
	}
	
	public float getWorldHeight() {
		return (float)(viewportHeight*dZoom);
	}
	
	public float getAspectRatio() {
		return viewportWidth/viewportHeight;
	}
	
	@Override
	public void update() {
		position.x = (float) x;
		position.y = (float) y;
		zoom = (float) dZoom;
		super.update();
	}
	
}
