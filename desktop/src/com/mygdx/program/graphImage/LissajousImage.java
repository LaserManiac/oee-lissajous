package com.mygdx.program.graphImage;

import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.StreamUtils;
import com.mygdx.program.Lissajous;
import com.mygdx.program.saving.FileChooser;
import com.mygdx.program.signal.ExpressionSignal;
import com.mygdx.program.signal.Signal;
import com.mygdx.program.signalScreen.ImageSavedMessage;

public class LissajousImage extends GraphImage {
	
	private static final double ERROR_MARGIN = 0.0001;
	
	public static double getGreatestCommonDivisor(double a, double b) {
		if(Math.abs(b) < ERROR_MARGIN)
			return a;
		else
			return getGreatestCommonDivisor(b, a%b);
	}
	
	public static double getLeastCommonMultiple(double a, double b) {
		return Math.abs(a*b) / getGreatestCommonDivisor(a, b);
	}
	
	public static int PLOT_RESOLUTION = 10000;
	
	private final Table toolbar;
	
	public LissajousImage() {
		super();
		
		toolbar = new Table(Lissajous.skin);
		toolbar.pad(4);
		toolbar.setSize(200, 50);
		toolbar.setBackground("textfieldLight");
		
		Label resLabel = new Label("Rezolucija:", Lissajous.skin);
		toolbar.add(resLabel).padRight(4).padLeft(4);
		
		final SelectBox<Integer> resolution = new SelectBox<Integer>(Lissajous.skin, "light");
		resolution.setItems(1000, 5000, 10000, 15000, 20000, 25000, 30000, 40000, 50000);
		resolution.setSelected(PLOT_RESOLUTION);
		resolution.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				PLOT_RESOLUTION = resolution.getSelected();
				requestReplot();
				requestRedraw();
			}
		});
		toolbar.add(resolution);
		
		ImageButton save = new ImageButton(Lissajous.skin, "save");
		save.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				getStage().addActor(new FileChooser(true, ".lissajous/saves/", ".graph", (file) -> {
					try {
						
						Writer writer = file.writer(false);
						Json json = new Json();
						json.setWriter(writer);
						
						json.writeObjectStart();
						
						json.writeObjectStart("signal_x");
						Lissajous.signal1.toJson(json);
						json.writeObjectEnd();
						
						json.writeObjectStart("signal_y");
						Lissajous.signal2.toJson(json);
						json.writeObjectEnd();
						
						json.writeObjectEnd();
						
						StreamUtils.closeQuietly(writer);
						
					} catch(Exception e) {
						System.out.println("ERROR saving graph: " + e.getMessage());
					}
				}));
			}
		});
		toolbar.add(save).padRight(5);
		
		ImageButton load = new ImageButton(Lissajous.skin, "load");
		load.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				getStage().addActor(new FileChooser(false, ".lissajous/saves/", ".graph", (file) -> {
					try {
						
						JsonReader reader = new JsonReader();
						JsonValue data = reader.parse(file);
						
						JsonValue signal1 = data.get("signal_x");
						Lissajous.signal1.fromJson(signal1);
						
						JsonValue signal2 = data.get("signal_y");
						Lissajous.signal2.fromJson(signal2);
					
						center();
						
					} catch(Exception e) {
						System.out.println("ERROR loading graph: " + e.getMessage());
					}
				}));
			}
		});
		toolbar.add(load).padRight(4);
		
		ImageButton camera = new ImageButton(Lissajous.skin, "camera");
		camera.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
				String name = "graph " + format.format(new Date());
				FileHandle file = snapshot(name);
				getStage().addActor(new ImageSavedMessage("SLIKA GRAFA SPREMLJENA!", file));
			}
		});
		toolbar.add(camera).padRight(4);
		
		ImageButton grid = new ImageButton(Lissajous.skin, "grid_0");
		grid.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				GridRenderMode mode = GridRenderMode.values()[(getGridRenderMode().ordinal()+1)%3];
				setGridRenderMode(mode);
				requestRedraw();
				grid.setStyle(Lissajous.skin.get("grid_" + mode.ordinal(), ImageButtonStyle.class));
			}
		});
		toolbar.add(grid).padRight(4);
		
		Container<Table> container = new Container<Table>(toolbar);
		container.setFillParent(true);
		container.left().top();
		addActor(container);
	}
	
	private double[] polygon;
	private double minX = -1, minY = -1, maxX = 1, maxY = 1;
	protected synchronized void updateData(double[] polygon, double minX, double minY, double maxX, double maxY) {
		this.polygon = polygon;
		this.minX = minX;
		this.minY = minY;
		this.maxX = maxX;
		this.maxY = maxY;
	}
	
	@Override
	protected void plotGraph() {
		polygon = new double[PLOT_RESOLUTION*2];
		Signal s1 = Lissajous.signal1.getSignal();
		Signal s2 = Lissajous.signal2.getSignal();
		
		double interval;
		if(s1.isPeriodic() && s2.isPeriodic()) interval = getLeastCommonMultiple(s1.providePlotInterval(), s2.providePlotInterval());
		else if(s1.isPeriodic()) interval = s2.providePlotInterval();
		else if(s2.isPeriodic()) interval = s1.providePlotInterval();
		else interval = Math.min(s1.providePlotInterval(), s2.providePlotInterval());
		
		if(s1 instanceof ExpressionSignal && s2 instanceof ExpressionSignal) {
			
			((ExpressionSignal)s1).begin();
			for(int i = 0; i < polygon.length; i += 2) {
				double t = interval*i/polygon.length;
				double x = s1.evaluate(t);
				polygon[i+0] = x;
				
				if(i==0) {
					minX = x;
					maxX = x;
				} else {
					minX = Math.min(minX, x);
					maxX = Math.max(maxX, x);
				}
			}
			((ExpressionSignal)s1).end();
			
			((ExpressionSignal)s2).begin();
			for(int i = 0; i < polygon.length; i += 2) {
				double t = interval*i/polygon.length;
				double y = s2.evaluate(t);
				polygon[i+1] = y;
				
				if(i==0) {
					minY = y;
					maxY = y;
				} else {
					minY = Math.min(minY, y);
					maxY = Math.max(maxY, y);
				}
			}
			((ExpressionSignal)s2).end();
			
		} else {
			
			if(s1 instanceof ExpressionSignal) ((ExpressionSignal)s1).begin();
			if(s2 instanceof ExpressionSignal) ((ExpressionSignal)s2).begin();
			
			for(int i = 0; i < polygon.length; i += 2) {
				double t = interval*i/polygon.length;
				double x = s1.evaluate(t);
				double y = s2.evaluate(t);
				polygon[i+0] = x;
				polygon[i+1] = y;
				
				if(i==0) {
					minX = x;
					minY = y;
					maxX = x;
					maxY = y;
				} else {
					minX = Math.min(minX, x);
					minY = Math.min(minY, y);
					maxX = Math.max(maxX, x);
					maxY = Math.max(maxY, y);
				}
			}
			
			if(s1 instanceof ExpressionSignal) ((ExpressionSignal)s1).end();
			if(s2 instanceof ExpressionSignal) ((ExpressionSignal)s2).end();
			
		}
		
	}

	@Override
	protected synchronized void drawGraph(ShapeRenderer renderer) {
		if(polygon == null) return;
		int size = polygon.length/2;
		renderer.setColor(Color.BLUE);
		for(int i = 0; i < size-1; i++) {
			float x1 = (float)(polygon[i*2+0]);
			float y1 = (float)(polygon[i*2+1]);
			float x2 = (float)(polygon[i*2+2]);
			float y2 = (float)(polygon[i*2+3]);
			renderer.line(x1, y1, x2, y2);
		}
	}

	@Override
	protected void center() {
		camTargetX = (minX+maxX)/2;
		camTargetY = (minY+maxY)/2;
		double width = Math.min(maxX-minX, 1000);
		double height = Math.min(maxY-minY, 1000);
		camTargetZoom = Math.max(width/getWidth()*1.5, height/getHeight()*1.5);
	}
	
}
