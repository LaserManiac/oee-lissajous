package com.mygdx.program.graphImage;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygdx.program.Lissajous;
import com.mygdx.program.signal.ExpressionSignal;
import com.mygdx.program.signal.Signal;
import com.mygdx.program.signalScreen.NumericTextfield;
import com.mygdx.program.signalScreen.SignalScreen;

public class SignalImage extends GraphImage {
	
	private static final int GRAPH_PLOT_RESOLUTION = 500;
	private static final Color GRAPH_COLOR = new Color(Color.BLUE);
	
	private Signal signal;
	public Signal getSignal() {
		return signal;
	}
	public void setSignal(Signal signal) {
		this.signal = signal;
	}
	
	public SignalImage() {
		setGridBase(10.0);
	}

	public void updateWidgets() {
		SignalScreen<?> parent = (SignalScreen<?>) getParent();
		for(NumericTextfield field : parent.getFields()) field.updateData();
	}
	
	@Override
	protected void plotGraph() {
		updateWidgets();
		
		Grid grid = getGrid();
		double major = grid.getMajorSize();
		double gb = grid.getBase();
		int halfChunksCovered = (int) Math.floor(getWidth()/(gb*gb)/2);
		double x = (gridChunkX-halfChunksCovered) * major;
		double w = (halfChunksCovered*2+1) * major;
		int resolution = GRAPH_PLOT_RESOLUTION*halfChunksCovered*2;

		if(signal instanceof ExpressionSignal) ((ExpressionSignal)signal).begin();
		signal.plot(x, x+w, resolution);
		if(signal instanceof ExpressionSignal) ((ExpressionSignal)signal).end();
	}
	
	@Override
	protected void drawGraph(ShapeRenderer renderer) {
		renderer.setColor(GRAPH_COLOR);
		signal.render(Lissajous.renderer);
	}
	
	public void center() {
		double x = signal.provideCenterX();
		double y = signal.provideCenterY();
		double width = signal.provideCenterWidth();
		double height = signal.provideCenterHeight();
		
		camTargetZoom = Math.max(width/getWidth(), height/getHeight()*1.5);
		camTargetX = x + camTargetZoom*getWidth()/2;
		camTargetY = y;
		camera.update();
	}
	
}
