package com.mygdx.program.graphImage;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.RenderableProvider;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Pool;

public class Grid implements RenderableProvider, Disposable {
	
	public static final Color MAJOR_COLOR = new Color(Color.WHITE).mul(0.6f);
	public static final Color MINOR_COLOR = new Color(Color.WHITE).mul(0.8f);
	
	private Model model;
	private ModelInstance minorInstance, majorInstance;
	
	// TODO: Make it possible to have different grid subdivision sizes (eg. 2*PI)
	private double base;
	public double getBase() {
		return base;
	}
	public void setBase(double base) {
		this.base = base;
	}
	
	private int level;
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}

	private boolean hasOffsetX, hasOffsetY;
	
	public double getSize(int level) {
		return Math.pow(base, level+1);
	}
	public double getMinorSize() {
		return getSize(level+1);
	}
	public double getMajorSize() {
		return getSize(level+2);
	}
	
	public void setModelSize(float width, float height) {
		if(model != null) model.dispose();
		
		ModelBuilder mb = new ModelBuilder();
		int xSegments = (int)(width/base)+2;
		int ySegments = (int)(height/base)+2;
		hasOffsetX = (xSegments&1) == 1;
		hasOffsetY = (ySegments&1) == 1;
		model = mb.createLineGrid(xSegments, ySegments, 1, 1, new Material(ColorAttribute.createDiffuse(Color.RED)), Usage.Position | Usage.ColorPacked);
		majorInstance = new ModelInstance(model);
		minorInstance = new ModelInstance(model);
		((ColorAttribute)minorInstance.materials.first().get(ColorAttribute.Diffuse)).color.set(MINOR_COLOR);
		((ColorAttribute)majorInstance.materials.first().get(ColorAttribute.Diffuse)).color.set(MAJOR_COLOR);
		//System.out.println("Grid model size: " + xSegments + " " + ySegments);
	}
	
	public void set(float x, float y) {
		float minorSize = (float) getMinorSize();
		float minX = (int) Math.floor(x/minorSize)*minorSize - (float) (hasOffsetX ? minorSize/2 : 0);
		float minY = (int) Math.floor(y/minorSize)*minorSize + (float) (hasOffsetY ? minorSize/2 : 0);
		minorInstance.transform.setToTranslation(minX, minY, -0.5f);
		minorInstance.transform.rotate(Vector3.X, 90);
		minorInstance.transform.scale(minorSize, minorSize, minorSize);
		
		float majorSize = (float) getMajorSize();
		float majX = (int) Math.floor(x/majorSize)*majorSize - (float) (hasOffsetX ? majorSize/2 : 0);
		float majY = (int) Math.floor(y/majorSize)*majorSize + (float) (hasOffsetY ? majorSize/2 : 0);
		majorInstance.transform.setToTranslation(majX, majY, -0.5f);
		majorInstance.transform.rotate(Vector3.X, 90);
		majorInstance.transform.scale(majorSize, majorSize, majorSize);
	}

	@Override
	public void getRenderables(Array<Renderable> renderables, Pool<Renderable> pool) {
		minorInstance.getRenderables(renderables, pool);
		majorInstance.getRenderables(renderables, pool);
	}

	@Override
	public void dispose() {
		if(model != null) model.dispose();
	}
	
}
