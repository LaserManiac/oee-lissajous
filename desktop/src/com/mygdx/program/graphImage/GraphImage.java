package com.mygdx.program.graphImage;

import java.nio.ByteBuffer;

import org.lwjgl.opengl.GL11;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Disposable;
import com.mygdx.program.Lissajous;

public abstract class GraphImage extends WidgetGroup implements Disposable {

	public static final float INTERPOLATION_FACTOR = 10f;
	public static final float INTERPOLATION_MIN_VALUE = 0.001f;
	
	public static final double ZOOM_MIN = 5e-7;
	public static final double ZOOM_MAX = 200.0;
	
	public static final Color BACKGROUND_COLOR = new Color(Color.WHITE).mul(0.9f);
	public static final Color AXIS_COLOR = new Color(Color.WHITE).mul(0.05f);
	
	private final Drawable background;
	
	private FrameBuffer fbo;
	public FrameBuffer getFBO() {
		return fbo;
	}
	
	private Image fboImage;
	
	protected GraphImageCamera camera;
	protected double camTargetX = 0, camTargetY = 0, camTargetZoom = 0.001;	
	public GraphImageCamera getCamera() {
		return camera;
	}
	
	public double toWorldX(double x) {
		return camera.x + (x - (getX()+getWidth()/2))*camera.zoom;
	}
	public double toWorldY(double y) {
		return camera.y + (y - (getY()+getHeight()/2))*camera.zoom;
	}
	public double toScreenX(double x) {
		return (x - camera.x)/camera.zoom + (getX()+getWidth()/2);
	}
	public double toScreenY(double y) {
		return (y - camera.y)/camera.zoom + (getY()+getHeight()/2);
	}
	
	private Grid grid;
	protected int gridChunkX;
	public Grid getGrid() {
		return grid;
	}
	public void setGridBase(double newGridBase) {
		grid.setBase(newGridBase);
		grid.setModelSize(getWidth(), getHeight());
	}
	
	public enum GridRenderMode {
		ALL, AXIS, NONE;
	}
	private GridRenderMode gridRenderMode = GridRenderMode.ALL;
	public GridRenderMode getGridRenderMode() {
		return gridRenderMode;
	}
	public void setGridRenderMode(GridRenderMode mode) {
		gridRenderMode = mode;
	}
	
	private boolean needsReplot = true;
	public boolean isReplotRequested() {
		return needsReplot;
	}
	public void requestReplot() {
		needsReplot = true;
	}
	protected void replot() {
		plotGraph();
		needsReplot = false;
	}
	protected abstract void plotGraph();
	
	private boolean needsRedraw = true;
	public void requestRedraw() {
		needsRedraw = true;
	}
	private void redraw() {
		if(fbo==null) {
			adjustSize();
			center();
		}
		
		fbo.begin();
		
		// Clear screen
		Gdx.gl.glClearColor(BACKGROUND_COLOR.r, BACKGROUND_COLOR.g, BACKGROUND_COLOR.b, BACKGROUND_COLOR.a);
		Gdx.gl.glClear(GL11.GL_COLOR_BUFFER_BIT);
		
		// Just some vars
		float x = camera.position.x;
		float y = camera.position.y;
		float w = camera.getWorldWidth();
		float h = camera.getWorldHeight();
		
		// Render grid
		if(gridRenderMode == GridRenderMode.ALL) {
			grid.set((float) camera.x, (float) camera.y);
			ModelBatch batch = Lissajous.modelBatch;
			batch.begin(camera);
			batch.render(grid);
			batch.end();
		}
		
		// Set up shape renderer
		ShapeRenderer renderer = Lissajous.renderer;
		renderer.setProjectionMatrix(camera.combined);
		renderer.begin(ShapeType.Line);
		
		// Render main axis
		if(gridRenderMode == GridRenderMode.ALL || gridRenderMode == GridRenderMode.AXIS) {
			renderer.setColor(AXIS_COLOR);
			renderer.line(0, y - h/2, 0, y + h/2);
			renderer.line(x - w/2, 0, x + w/2, 0);
		}
		
		// Render graph
		drawGraph(renderer);
		
		renderer.end();
		fbo.end();
		
		needsRedraw = false;
		//System.out.println("REDRAW");
	}
	protected abstract void drawGraph(ShapeRenderer renderer);
	
	public GraphImage() {
		camera = new GraphImageCamera();
		grid = new Grid();
		background = Lissajous.skin.getDrawable("border");
		
		fboImage = new Image();
		fboImage.setFillParent(true);
		fboImage.addListener(new GridScreenListener());
		addActor(fboImage);
		
		addListener(new InputListener() {
			@Override
			public void enter (InputEvent event, float x, float y, int pointer, Actor fromActor) {
				getStage().setScrollFocus(fboImage);
				toFront();
			}
		});
	}
	
	protected abstract void center();
	
	private void adjustSize() {
		if(fbo != null) fbo.dispose();
		
		int width = (int)Math.max(1, getWidth());
		int height = (int)Math.max(1, getHeight());
		
		fbo = new FrameBuffer(Format.RGB888, width, height, false, false);
		Lissajous.checkForGlErrors("FunctionScreen fbo creation");
		
		TextureRegion region = new TextureRegion(fbo.getColorBufferTexture());
		region.flip(false, true);
		fboImage.setDrawable(new TextureRegionDrawable(region));
		
		camera.setViewportSize(width, height);
		grid.setModelSize(width, height);
		
		requestReplot();
		requestRedraw();
	}
	
	@Override
	public void sizeChanged() {
		super.sizeChanged();
		adjustSize();
		center();
	}
	
	@Override
	public void act(float delta) {
		/* Interpolate camera values */ {
			
			double camDiff = camTargetZoom-camera.dZoom;
			if(Math.abs(camDiff) > INTERPOLATION_MIN_VALUE*camera.zoom) {
				camera.dZoom += camDiff*delta*INTERPOLATION_FACTOR;
				requestRedraw();
			} else camera.dZoom = camTargetZoom;
			
			double xDiff = camTargetX-camera.x;
			if(Math.abs(xDiff) > INTERPOLATION_MIN_VALUE*camera.zoom) {
				camera.x += xDiff*delta*INTERPOLATION_FACTOR;
				requestRedraw();
			} else camera.x = camTargetX;
			
			double yDiff = camTargetY-camera.y;
			if(Math.abs(yDiff) > INTERPOLATION_MIN_VALUE*camera.zoom) {
				camera.y += yDiff*delta*INTERPOLATION_FACTOR;
				requestRedraw();
			} else camera.y = camTargetY;
		}
		
		/* Update grid and function graph */ {
			int newGridLevel = (int) Math.floor(Math.log10(camera.dZoom)/Math.log10(grid.getBase()));
			if(newGridLevel != grid.getLevel()) {
				grid.setLevel(newGridLevel);
				if(this instanceof SignalImage) requestReplot();
				requestRedraw();
			}
			
			int newChunkX = (int) Math.floor(camera.x/grid.getMajorSize());
			if(newChunkX != gridChunkX) {
				gridChunkX = newChunkX;
				if(this instanceof SignalImage) requestReplot();
				requestRedraw();
			}
		}
		
		if(needsRedraw) camera.update();
		super.act(delta);
		
		if(needsReplot) replot();
		if(needsRedraw) redraw();
	}
	
	@Override
	public void draw (Batch batch, float parentAlpha) {
		boolean clip = !(getWidth() <= 0 || getHeight() <= 0);
		if(clip) {
			batch.flush();
			clipBegin(getX(), getY(), getWidth(), getHeight());
		}
		super.draw(batch, parentAlpha);
		if(clip) clipEnd();
		background.draw(batch, getX(), getY(), getWidth(), getHeight());
	}
	
	public FileHandle snapshot(String name) {

		// Create dummy FBO
		FrameBuffer fbo = new FrameBuffer(Format.RGB888, getFBO().getWidth(), getFBO().getHeight(), false, false);
		
		// Copy image texture to dummy fbo, but implicitly invert Y because SpriteBatch uses a different coord system than OpenGL
		fbo.begin();
		Lissajous.spriteBatch.getProjectionMatrix().setToOrtho2D(0, 0, fbo.getWidth(), fbo.getHeight());
		Lissajous.spriteBatch.begin();
		Lissajous.spriteBatch.draw(getFBO().getColorBufferTexture(), 0, 0);
		Lissajous.spriteBatch.end();
		fbo.end();
		
		// Create tmp pixmap to download data into
		Pixmap data = new Pixmap(fbo.getWidth(), fbo.getHeight(), Format.RGB888);
		ByteBuffer pixels = data.getPixels();
		
		// Bind texture
		Texture texture = fbo.getColorBufferTexture();
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture.getTextureObjectHandle());
		
		// Set unpack alignment, because out texture size is not a power of 2
		GL11.glPixelStorei(GL11.GL_PACK_ALIGNMENT, 1);
		
		// Download pixel data
		GL11.glGetTexImage(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, GL11.GL_UNSIGNED_BYTE, pixels);
		
		// Reset unpack alignment
		GL11.glPixelStorei(GL11.GL_PACK_ALIGNMENT, 4);
		
		// Check for errors
		Lissajous.checkForGlErrors("Image Saving"); 
		
		// Save image
		FileHandle file = Gdx.files.external(".lissajous/images/" + name + ".png");
		int n = 1;
		while(file.exists()) file = Gdx.files.external(".lissajous/images/" + name + " (" + (n++) + ")" + ".png");
		PixmapIO.writePNG(file, data);
		
		// Dispose tmp data
		fbo.dispose();
		data.dispose();
		
		// Debug
		System.out.println("Created image snapshot at \"" + file.toString() + "\"");
		
		// Return FileHandle for saved image file
		return file;
	}
	
	@Override
	public void dispose() {
		fbo.dispose();
		grid.dispose();
	}

	private class GridScreenListener extends InputListener {
		
		int dragging = -1;
		float dragX, dragY;
		
		public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
			if(button == Buttons.LEFT && dragging == -1) {
				dragging = pointer;
				dragX = x;
				dragY = y;
				return true;
			}
			else if(button == Buttons.RIGHT) {
				center();
				return true;
			}
			return false;
		}
		
		public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
			if(button == Buttons.LEFT && pointer == dragging) dragging = -1;
		}
		
		public void touchDragged (InputEvent event, float x, float y, int pointer) {
			if(pointer == dragging) {
				double diffX = (x-dragX)*(camera.viewportWidth*camera.dZoom)/getWidth();
				double diffY = (y-dragY)*(camera.viewportHeight*camera.dZoom)/getHeight();
				camTargetX -= diffX;
				camTargetY -= diffY;
				dragX = x;
				dragY = y;
				needsRedraw = true;
			}
		}
		
		public boolean scrolled (InputEvent event, float x, float y, int amount) {
			double old = camTargetZoom;
			camTargetZoom *= Math.pow(1.2, amount);
			
			if(camTargetZoom > ZOOM_MAX) camTargetZoom = ZOOM_MAX;
			if(camTargetZoom < ZOOM_MIN) camTargetZoom = ZOOM_MIN;
			
			double diff = camTargetZoom - old;
			camTargetX -= (x - getWidth()/2)*diff;
			camTargetY -= (y - getHeight()/2)*diff;
			
			//System.out.println("Zoom: " + camTargetZoom);
			
			needsRedraw = true;
			return true;
		}
		
	}
	
}
