package com.mygdx.program.expression;

import java.util.HashMap;
import java.util.Map.Entry;

import com.mygdx.program.expression.member.Member;
import com.mygdx.program.signal.Parameter;

public final class Expression {
	
	private final Member last;
	public double evaluate() {
		if(context == null) throw new RuntimeException("Can't evaluate expression with no context!");
		return last.evaluate();
	}
	
	private final HashMap<String, ParameterLink> parameters;
	private final ParameterLink argument;
	
	private ExpressionContext context;
	public void linkContext(ExpressionContext context) {
		if(this.context == context) return;
		
		this.context = context;
		
		if(context != null) {
			argument.setParameter(context.getArgument());
			for(Entry<String, ParameterLink> entry : parameters.entrySet()) {
				Parameter parameter = context.getParameter(entry.getKey());
				entry.getValue().setParameter(parameter);
			}
		}
	}
	public void looseContext() {
		context = null;
		argument.setParameter(null);
		for(Entry<String, ParameterLink> entry : parameters.entrySet()) {
			entry.getValue().setParameter(null);
		}
	}
	
	public Expression(Member last, ParameterLink argument, HashMap<String, ParameterLink> parameters) {
		this.last = last;
		this.argument = argument;
		this.parameters = parameters;
	}
	
}
