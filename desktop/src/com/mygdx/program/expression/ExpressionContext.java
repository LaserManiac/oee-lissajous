package com.mygdx.program.expression;

import com.mygdx.program.signal.Parameter;

public interface ExpressionContext {
	
	public boolean isPeriodic();
	public double getPeriod();
	public Parameter getParameter(String name);
	public Parameter getArgument();
	
}
