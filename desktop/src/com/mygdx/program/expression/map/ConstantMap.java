package com.mygdx.program.expression.map;

import java.util.HashMap;

import com.mygdx.program.expression.Constant;

public class ConstantMap extends HashMap<String, Constant> {

	private static final long serialVersionUID = 1L;
	
	public ConstantMap add(String name, double value) {
		put(name, new Constant(value));
		return this;
	}

}
