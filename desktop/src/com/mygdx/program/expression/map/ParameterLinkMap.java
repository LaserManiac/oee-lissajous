package com.mygdx.program.expression.map;

import java.util.HashMap;

import com.mygdx.program.expression.ParameterLink;

public class ParameterLinkMap extends HashMap<String, ParameterLink> {

	private static final long serialVersionUID = 1L;
	
	public ParameterLinkMap add(String name) {
		put(name, new ParameterLink());
		return this;
	}

}