package com.mygdx.program.expression.map;

import java.util.HashMap;

import com.mygdx.program.expression.MacroBlueprint;

public class MacroMap extends HashMap<String, MacroBlueprint> {
	
	private static final long serialVersionUID = 1L;
	
	public MacroMap add(String name, String expression, String... arguments) {
		put(name, new MacroBlueprint(name, expression, arguments));
		return this;
	}
	
}
