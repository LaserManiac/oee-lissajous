package com.mygdx.program.expression.map;

import java.util.HashMap;

import com.mygdx.program.expression.Expression;
import com.mygdx.program.expression.Variable;

public class VariableMap extends HashMap<String, Variable> {
	
	private static final long serialVersionUID = 1L;
	
	public VariableMap add(String name, Expression expression) {
		put(name, new Variable(expression));
		return this;
	}

}
