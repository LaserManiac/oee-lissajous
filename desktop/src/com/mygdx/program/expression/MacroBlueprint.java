package com.mygdx.program.expression;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MacroBlueprint {
	
	private final List<MacroArgument> arguments = new ArrayList<>();
	public int numOfArgs() {
		return arguments == null ? 0 : arguments.size();
	}
	public void setArgument(int i, String value) {
		arguments.get(i).value = value;
	}
	public MacroArgument getArgument(String name) {
		for(MacroArgument argument : arguments)
			if(argument.name.equals(name))
				return argument;
		return null;
	}
	
	private final List<Object> buildList = new ArrayList<>();
	public String build() {
		StringBuilder builder = new StringBuilder();
		builder.append("(");
		buildList.forEach((obj) -> builder.append(obj.toString()));
		builder.append(")");
		return builder.toString();
	}
	
	private final static Pattern pat_Argument = Pattern.compile("%([A-Za-z]+[A-Za-z0-9]*)");
	
	public MacroBlueprint(String macroName, String expression, String... arguments) {
		if(expression.matches(".*" + macroName + "\\(.*")) {
			throw new RuntimeException("Makro funkcija ne smije zvati samu sebe! (Rekurzija nije dozvoljena)");
		}
		
		if(arguments != null)
			for(String argument : arguments)
				this.arguments.add(new MacroArgument(argument));
		
		String tmpXpr = expression;
		Matcher matcher = pat_Argument.matcher(tmpXpr);
		while(matcher.find()) {
			String name = matcher.group(1);
			MacroArgument arg = getArgument(name);
			if(arg == null) throw new RuntimeException("Makro argument \"" + name + "\" ne postoji!");
			buildList.add(tmpXpr.substring(0, matcher.start()));
			buildList.add(arg);
			tmpXpr = tmpXpr.substring(matcher.end());
			matcher.reset(tmpXpr);
		}
		if(!tmpXpr.isEmpty()) buildList.add(tmpXpr);
	}
	
	@Override
	public String toString() {
		return build();
	}
	
	private static final class MacroArgument {
		
		public final String name;
		public String value = "MISSING_VALUE";
		
		public MacroArgument(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return "(" + value + ")";
		}
		
	}
	
}
