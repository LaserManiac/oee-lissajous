package com.mygdx.program.expression;

public final class Variable {

	private final Expression expression;
	public void update(ExpressionContext context) {
		expression.linkContext(context);
		value = expression.evaluate();
	}
	
	private double value;
	public double get() {
		return value;
	}
	
	public Variable(Expression expression) {
		this.expression = expression;
	}
	
}
