package com.mygdx.program.expression;

import com.mygdx.program.signal.Parameter;

public final class ParameterLink {

	private Parameter parameter;
	public void setParameter(Parameter parameter) {
		this.parameter = parameter;
	}
	
	public double get() {
		return parameter.get();
	}
	
}
