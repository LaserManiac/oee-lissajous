package com.mygdx.program.expression;

import java.util.ArrayList;
import java.util.List;

import com.mygdx.program.expression.member.Member;

public final class FunctionBlueprint {
	
	public static final List<FunctionBlueprint> blueprints = new ArrayList<>();
	
	static {
		blueprints.add(new FunctionBlueprint("sin", 1, (args) -> Math.sin(args[0].evaluate()) ));
		blueprints.add(new FunctionBlueprint("cos", 1, (args) -> Math.cos(args[0].evaluate()) ));
		blueprints.add(new FunctionBlueprint("tg", 1, (args) -> Math.tan(args[0].evaluate()) ));
		blueprints.add(new FunctionBlueprint("ctg", 1, (args) -> 1.0/Math.tan(args[0].evaluate()) ));
		blueprints.add(new FunctionBlueprint("toDeg", 1, (args) -> Math.toDegrees(args[0].evaluate()) ));
		blueprints.add(new FunctionBlueprint("toRad", 1, (args) -> Math.toRadians(args[0].evaluate()) ));
		
		blueprints.add(new FunctionBlueprint("sign", 1, (args) -> Math.signum(args[0].evaluate()) ));
		blueprints.add(new FunctionBlueprint("abs", 1, (args) -> Math.abs(args[0].evaluate()) ));
		blueprints.add(new FunctionBlueprint("sqr", 1, (args) -> { double x = args[0].evaluate(); return x*x; } ));
		blueprints.add(new FunctionBlueprint("sqrt", 1, (args) -> Math.sqrt(args[0].evaluate()) ));
		blueprints.add(new FunctionBlueprint("pow", 2, (args) -> Math.pow(args[0].evaluate(), args[1].evaluate()) ));
		blueprints.add(new FunctionBlueprint("log", 2, (args) -> Math.log(args[0].evaluate())/Math.log(args[1].evaluate()) ));
		blueprints.add(new FunctionBlueprint("mod", 2, (args) -> args[0].evaluate()%args[1].evaluate() ));
		blueprints.add(new FunctionBlueprint("div", 2, (args) -> { double a = args[0].evaluate(); double b = args[1].evaluate(); double f = Math.floor(a/b); return f<0?f+1:f; } ));
		
		blueprints.add(new FunctionBlueprint("int", 1, (args) -> (int) args[0].evaluate() ));
		blueprints.add(new FunctionBlueprint("round", 1, (args) -> Math.round(args[0].evaluate()) ));
		blueprints.add(new FunctionBlueprint("floor", 1, (args) -> Math.floor(args[0].evaluate()) ));
		blueprints.add(new FunctionBlueprint("ceil", 1, (args) -> Math.ceil(args[0].evaluate()) ));
		blueprints.add(new FunctionBlueprint("trunc", 1, (args) -> { double x = args[0].evaluate(); return x - (int) x; } ));
		
		blueprints.add(new FunctionBlueprint("max", 2, (args) -> Math.max(args[0].evaluate(), args[1].evaluate()) ));
		blueprints.add(new FunctionBlueprint("min", 2, (args) -> Math.min(args[0].evaluate(), args[1].evaluate()) ));
		
		blueprints.add(new FunctionBlueprint("isEqual", 3, (args) -> (Math.abs(args[0].evaluate()-args[1].evaluate())<=args[2].evaluate())?1:0 ));
		blueprints.add(new FunctionBlueprint("isLarger", 2, (args) -> args[0].evaluate()>args[1].evaluate()?1:0 ));
		blueprints.add(new FunctionBlueprint("isLargerEqual", 2, (args) -> args[0].evaluate()>=args[1].evaluate()?1:0 ));
		blueprints.add(new FunctionBlueprint("isSmaller", 2, (args) -> args[0].evaluate()<args[1].evaluate()?1:0 ));
		blueprints.add(new FunctionBlueprint("isSmallerEqual", 2, (args) -> args[0].evaluate()<=args[1].evaluate()?1:0 ));
		blueprints.add(new FunctionBlueprint("if", 3, (args) -> args[0].evaluate()==0?args[2].evaluate():args[1].evaluate()));
	}
	
	public final String name;
	public final int argumentCount;
	public final Function function;
	
	public FunctionBlueprint(String name, int argumentCount, Function function) {
		this.name = name;
		this.argumentCount = argumentCount;
		this.function = function;
	}

	public static interface Function {
		public double run(Member[] args);
	}

}
