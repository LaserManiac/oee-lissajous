package com.mygdx.program.expression;

import com.mygdx.program.expression.member.Member;

public final class Link {
	
	private final Member previous;
	private final OperatorBlueprint operator;
	
	public Link(Member previous, OperatorBlueprint operator) {
		this.previous = previous;
		this.operator = operator;
	}
	
	public double evaluate(double value) {
		return operator.operation.perform(previous.evaluate(), value);
	}
	
}
