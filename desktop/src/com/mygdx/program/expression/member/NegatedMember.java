package com.mygdx.program.expression.member;

public class NegatedMember extends GroupMember {

	public NegatedMember(Member inside) {
		super(inside);
	}

	@Override
	protected double getValue() {
		return -super.getValue();
	}

}
