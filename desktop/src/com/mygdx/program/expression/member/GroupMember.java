package com.mygdx.program.expression.member;

public class GroupMember extends Member {

	private final Member inside;
	
	public GroupMember(Member inside) {
		this.inside = inside;
	}
	
	@Override
	protected double getValue() {
		return inside.evaluate();
	}

}
