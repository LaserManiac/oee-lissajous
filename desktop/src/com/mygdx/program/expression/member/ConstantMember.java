package com.mygdx.program.expression.member;

import com.mygdx.program.expression.Constant;

public class ConstantMember extends Member {
	
	private final Constant value;
	
	public ConstantMember(Constant value) {
		this.value = value;
	}
	
	@Override
	protected double getValue() {
		return value.get();
	}

}
