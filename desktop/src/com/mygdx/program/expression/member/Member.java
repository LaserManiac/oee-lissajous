package com.mygdx.program.expression.member;

import com.mygdx.program.expression.Link;

public abstract class Member {

	private Link link;
	public void setPrevious(Link link) {
		this.link = link;
	}
	
	protected abstract double getValue();
	
	public double evaluate() {
		double value = getValue();
		if(link == null) return value;
		return link.evaluate(value);
	}
	
}
