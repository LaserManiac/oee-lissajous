package com.mygdx.program.expression.member;

public class NumberMember extends Member {
	
	private final double value;
	
	public NumberMember(double value) {
		this.value = value;
	}
	
	@Override
	protected double getValue() {
		return value;
	}
}
