package com.mygdx.program.expression.member;

import com.mygdx.program.expression.Variable;

public class VariableMember extends Member {

	private final Variable variable;
	
	public VariableMember(Variable variable) {
		this.variable = variable;
	}
	
	@Override
	protected double getValue() {
		return variable.get();
	}

}
