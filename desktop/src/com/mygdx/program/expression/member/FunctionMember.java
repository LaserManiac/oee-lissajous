package com.mygdx.program.expression.member;

import com.mygdx.program.expression.FunctionBlueprint;

public class FunctionMember extends Member {

	private final FunctionBlueprint blueprint;
	private final Member[] arguments;
	
	public FunctionMember(FunctionBlueprint blueprint, Member... arguments) {
		this.blueprint = blueprint;
		this.arguments = arguments;
	}
	
	@Override
	protected double getValue() {
		return blueprint.function.run(arguments);
	}

}
