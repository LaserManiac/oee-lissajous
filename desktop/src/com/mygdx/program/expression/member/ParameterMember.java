package com.mygdx.program.expression.member;

import com.mygdx.program.expression.ParameterLink;

public class ParameterMember extends Member {
	
	private final ParameterLink parameter;
	
	public ParameterMember(ParameterLink parameter) {
		this.parameter = parameter;
	}
	
	@Override
	protected double getValue() {
		return parameter.get();
	}
	
}
