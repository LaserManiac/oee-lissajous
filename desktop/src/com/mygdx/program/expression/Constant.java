package com.mygdx.program.expression;

public final class Constant {

	private final double value;
	public double get() {
		return value;
	}
	
	public Constant(double value) {
		this.value = value;
	}
	
}
