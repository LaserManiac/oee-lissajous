package com.mygdx.program.expression;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mygdx.program.expression.map.ConstantMap;
import com.mygdx.program.expression.map.MacroMap;
import com.mygdx.program.expression.map.ParameterLinkMap;
import com.mygdx.program.expression.map.VariableMap;
import com.mygdx.program.expression.member.ConstantMember;
import com.mygdx.program.expression.member.FunctionMember;
import com.mygdx.program.expression.member.GroupMember;
import com.mygdx.program.expression.member.Member;
import com.mygdx.program.expression.member.NegatedMember;
import com.mygdx.program.expression.member.NumberMember;
import com.mygdx.program.expression.member.ParameterMember;
import com.mygdx.program.expression.member.VariableMember;

public final class ExpressionParser {

	private static final Pattern pat_BracketAndFunction = Pattern.compile("([a-zA-Z]+[a-zA-Z0-9]*)\\(([^\\(\\)]*)\\)|\\(([^\\(\\)]*)\\)");
	
	private ConstantMap constantTable = null;
	private VariableMap variableTable = null;
	private MacroMap macroTable = null;
	private ParameterLinkMap parameterTable = null;
	private HashMap<String, Member> groupTable = null;
	private List<Variable> usedVariableList = null;
	private ParameterLink argumentParam = null;
	private ParseMode mode = null;
	
	private boolean debug = false;
	public void setDebug(boolean debug) {
		this.debug = debug;
	}
	
	public Expression parse(String expressionString, ConstantMap constants, VariableMap variables, MacroMap macros, ParameterLinkMap parameters, ParseMode mode) {
		if(debug) System.out.println("\n======== Parse start: [" + expressionString + "] ========");
		
		constantTable = constants;
		variableTable = variables;
		macroTable = macros;
		parameterTable = parameters;
		groupTable = new HashMap<>();
		usedVariableList = new ArrayList<>();
		argumentParam = new ParameterLink();
		this.mode = mode;
		
		String tmpExpression = expressionString;
		
		// Split groups
		int bracketCount = 0;
		Matcher mat_Brackets = pat_BracketAndFunction.matcher(tmpExpression);
		while(mat_Brackets.find()) {
			
			String replaceStr, groupStr;
			
			// % function
			// @ group
			// $ parameter
			// # variable
			// ? argument in macro
			// ! argument in expression
			
			// Function
			if(mat_Brackets.group(2) != null) {
				
				String func = mat_Brackets.group(1);
				groupStr = mat_Brackets.group(2);
				String[] argString = groupStr.split(",");
				int numOfArgs = groupStr.isEmpty() ? 0 : argString.length;
				
				if(macroTable.containsKey(func)) {
					
					MacroBlueprint macro = macroTable.get(func);
					
					if(numOfArgs != macro.numOfArgs())
						throw new RuntimeException("Makro funkcija \"" + func + "\" ima " + macro.numOfArgs() + " argument(a), a ne " + numOfArgs);
					
					if(numOfArgs != 0)
						for(int i = 0; i < argString.length; i++)
							macro.setArgument(i, argString[i]);
					
					replaceStr = macro.build();
					
				} else {
					
					replaceStr = "?_"+ func +"_" + bracketCount++;
					
					FunctionBlueprint function = null;
					for(FunctionBlueprint blueprint : FunctionBlueprint.blueprints) {
						if(blueprint.name.equals(func)) {
							function = blueprint;
							break;
						}
					}
					
					if(function == null) throw new RuntimeException("Nepoznata funkcija: " + func);
					
					if(numOfArgs != function.argumentCount)
						throw new RuntimeException("Funkcija \"" + function.name + "\" ima " + function.argumentCount + " argument(a), a ne " + argString.length);
					
					Member[] args = new Member[numOfArgs];
					for(int i = 0; i < numOfArgs; i++) args[i] = parseMemberGroup(argString[i]);
					Member group = new FunctionMember(function, args);
					groupTable.put(replaceStr, group);
					
				}
				
			}
			
			// Normal group
			else {
				groupStr = mat_Brackets.group(3);
				replaceStr = "@_m_" + bracketCount++;
				Member group = parseMemberGroup(groupStr);
				groupTable.put(replaceStr, group);
			}
			
			tmpExpression = tmpExpression.substring(0, mat_Brackets.start()) + replaceStr + tmpExpression.substring(mat_Brackets.end());
			mat_Brackets.reset(tmpExpression);
		}
		
		Member first = parseMemberGroup(tmpExpression);
		Expression expression = new Expression(first, argumentParam, parameterTable);
		
		groupTable.clear();
		constantTable = null;
		variableTable = null;
		macroTable = null;
		parameterTable = null;
		usedVariableList = null;
		argumentParam = null;
		
		return expression;
	}
	
	private int indent = 0;
	
	private Member parseMemberGroup(String group) {
		
		// Remove white spaces
		String tmpGroup = group.trim();
		
		// Debug print
		String indentStr = "";
		for(int i = 0; i < indent; i++) indentStr += "   ";
		indent++;
		if(debug) System.out.println(indentStr + "[" + tmpGroup + "]");
		
		// Check for empty group
		if(tmpGroup.length() == 0) {
			throw new RuntimeException("Prazan clan u izrazu! Provjerite da negdje nema dupliciranih operatora...");
		}
		
		// If can split by operators
		for(List<OperatorBlueprint> level : OperatorBlueprint.blueprints) {
			if(countOperatorTokensForLevel(level, tmpGroup) != 0) {
				
				Member last = null;
				
				int begin = 0;
				OperatorBlueprint operator = null;
				for(int i = 0; i <= tmpGroup.length(); i++) {
					boolean isLast = i==tmpGroup.length();
					char c = !isLast ? tmpGroup.charAt(i) : ' ';
					for(OperatorBlueprint blueprint : level) {
						if(c == blueprint.token && !(i==0 && blueprint.token=='-') || isLast) {
							String cGroup = tmpGroup.substring(begin, i);
							Member current = parseMemberGroup(cGroup);
							if(last != null) current.setPrevious(new Link(last, operator));
							last = current;
							operator = blueprint;
							begin = i+1;
							break;
						}
					}
				}
				
				indent--;
				return new GroupMember(last);
				
			}
		}
		
		// Check if it's X
		if(tmpGroup.equals("!x")) {
			if(mode == ParseMode.STATIC) throw new RuntimeException("Statican izraz ne smije sadrzavati argument x!");
			indent--;
			return new ParameterMember(argumentParam);
		}
		
		// Check if it's negation
		if(tmpGroup.charAt(0) == '-') {
			Member groupMember = parseMemberGroup(tmpGroup.substring(1));
			indent--;
			return new NegatedMember(groupMember);
		}
		
		// If it's a group token
		if(tmpGroup.charAt(0) == '@') {
			Member groupMember = groupTable.get(tmpGroup);
			if(groupMember == null) throw new RuntimeException("Grupa " + tmpGroup + " ne postoji!");
			indent--;
			return new GroupMember(groupMember);
		}
		
		// If it's a function
		if(tmpGroup.charAt(0) == '?') {
			Member functionMember = groupTable.get(tmpGroup);
			if(functionMember == null) throw new RuntimeException("Funkcija " + tmpGroup + " ne postoji!");
			indent--;
			return new GroupMember(functionMember);
		}
		
		// If it's a parameter
		if(tmpGroup.charAt(0) == '$') {
			String paramStr = tmpGroup.substring(1);
			ParameterLink param = parameterTable.get(paramStr);
			if(param == null) throw new RuntimeException("Parametar " + tmpGroup + " ne postoji!");
			indent--;
			return new ParameterMember(param);
		}
		
		// If it's a variable
		if(tmpGroup.charAt(0) == '#') {
			String varStr = tmpGroup.substring(1);
			Variable var = variableTable.get(varStr);
			if(var == null) throw new RuntimeException("Variabla " + tmpGroup + " ne postoji!");
			usedVariableList.add(var);
			indent--;
			return new VariableMember(var);
		}
		
		// If it's a constant
		Constant constant = constantTable.get(tmpGroup);
		if(constant != null) {
			indent--;
			return new ConstantMember(constant);
		}
		
		// If it's a number
		try {
			double value = Double.parseDouble(tmpGroup);
			indent--;
			return new NumberMember(value);
		} catch(Exception e) {
			throw new RuntimeException("Konstanta " + tmpGroup + " ne postoji!");
		}
		
		//throw new RuntimeException("Could not parse: " + group);
	}
	
	private int countOperatorTokensForLevel(List<OperatorBlueprint> level, String str) {
		int opCount = 0;
		for(int i = 0; i < str.length(); i++) {
			for(OperatorBlueprint blueprint : level) {
				char c = str.charAt(i);
				if(c==blueprint.token && !(i==0 && c=='-')) {
					opCount++;
					break;
				}
			}
		}
		return opCount;
	}
	
	public static enum ParseMode {
		DYNAMIC,
		STATIC;
	}
	
}
