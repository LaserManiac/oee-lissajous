package com.mygdx.program.expression;

import java.util.ArrayList;
import java.util.List;

public final class OperatorBlueprint {
	
	public static final List<List<OperatorBlueprint>> blueprints = new ArrayList<>();
	
	static {
		List<OperatorBlueprint> level;
		
		level = new ArrayList<>();
		level.add(new OperatorBlueprint('|', (a, b) -> (a!=0 || b!=0) ? 1.0 : 0.0));
		blueprints.add(level);
		
		level = new ArrayList<>();
		level.add(new OperatorBlueprint('&', (a, b) -> (a!=0 && b!=0) ? 1.0 : 0.0));
		blueprints.add(level);
		
		level = new ArrayList<>();
		level.add(new OperatorBlueprint('>', (a, b) -> (a>b) ? 1.0 : 0.0));
		level.add(new OperatorBlueprint('<', (a, b) -> (a<b) ? 1.0 : 0.0));
		level.add(new OperatorBlueprint('=', (a, b) -> (a==b) ? 1.0 : 0.0));
		blueprints.add(level);
		
		level = new ArrayList<>();
		level.add(new OperatorBlueprint('+', (a, b) -> a+b));
		level.add(new OperatorBlueprint('-', (a, b) -> a-b));
		blueprints.add(level);
		
		level = new ArrayList<>();
		level.add(new OperatorBlueprint('*', (a, b) -> a*b));
		level.add(new OperatorBlueprint('/', (a, b) -> a/b));
		blueprints.add(level);
	}
	
	public final char token;
	public final Operation operation;
	
	public OperatorBlueprint(char token, Operation operation) {
		this.token = token;
		this.operation = operation;
	}
	
	public static interface Operation {
		public double perform(double a, double b);
	}
	
}
