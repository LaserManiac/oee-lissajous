package com.mygdx.program.resizeWidget;

import org.lwjgl.opengl.GL11;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.mygdx.program.Lissajous;
import com.mygdx.program.graphImage.GraphImage;
import com.mygdx.program.signalScreen.NumericTextfield;

public abstract class ResizeWidget extends Actor {
	
	protected double worldX, worldY;
	public double getWorldX() {
		return worldX;
	}
	public double getWorldY() {
		return worldY;
	}	
	public void setWorldX(double x) {
		worldX = x;
	}
	public void setWorldY(double y) {
		worldY = y;
	}
	
	protected boolean lockX, lockY;
	public ResizeWidget setLockX(boolean lockX) {
		this.lockX = lockX;
		return this;
	}
	public ResizeWidget setLockY(boolean lockY) {
		this.lockY = lockY;
		return this;
	}
	
	protected Color color;
	public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color.set(color);
	}
	
	protected final NumericTextfield textfield;
	
	public ResizeWidget(NumericTextfield textfield, Color color) {
		this.color = new Color(color);
		this.textfield = textfield;
		addListener(new ResizeWidgetListener());
		setWidth(16);
		setHeight(16);
		textfield.setWidget(this);
	}
	
	@Override
	public void act(float delta) {
		GraphImage parent = (GraphImage) getParent();
		float x = (float) parent.toScreenX(worldX) - parent.getX();
		float y = (float) parent.toScreenY(worldY) - parent.getY();
		align(x, y);
	}
	
	public abstract void align(float x, float y);
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		batch.end();
		
		ShapeRenderer renderer = Lissajous.renderer;
		renderer.setProjectionMatrix(batch.getProjectionMatrix());
		renderer.setColor(color);
		GL11.glLineWidth(2);
		renderer.begin(ShapeType.Line);
		drawShape(renderer);
		renderer.end();
		GL11.glLineWidth(1);
		
		batch.begin();
	}
	
	public abstract void drawShape(ShapeRenderer renderer);

	public abstract double worldToFunctionValue(double worldX, double worldY);
	
	public abstract void adjustWorldPosition(double x);
	
	private class ResizeWidgetListener extends InputListener {
		private float pressOffsetX, pressOffsetY;
		private int movePointer = -1;
		
		@Override
		public void enter (InputEvent event, float x, float y, int pointer, Actor fromActor) {
			//textfield.getStage().setKeyboardFocus(null);
			//textfield.getStage().setKeyboardFocus(textfield);
		}
		
		@Override
		public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
			if(button == Buttons.LEFT) {
				GraphImage parent = (GraphImage) getParent();
				if(x+getX()<0 || x+getX()>parent.getWidth() || y+getY()<0 || y+getY()>parent.getHeight()) return false;
				movePointer = pointer;
				pressOffsetX = x;
				pressOffsetY = y;
				
				textfield.getStage().setKeyboardFocus(null);
				textfield.getStage().setKeyboardFocus(textfield);
				
				return true;
			}
			return false;
		}
		
		@Override
		public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
			if(pointer == movePointer) {
				movePointer = -1;
			}
		}
		
		@Override
		public void touchDragged (InputEvent event, float x, float y, int pointer) {
			if(pointer == movePointer) {
				GraphImage parent = (GraphImage) getParent();
				boolean snap = Gdx.input.isKeyPressed(Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT);
				double size = parent.getGrid().getMinorSize();
				boolean update = false;
				
				double nwX = worldX;
				double nwY = worldY;
				
				float dx = x - pressOffsetX;
				if(!lockX && dx!=0) {
					nwX += dx*parent.getCamera().dZoom;
					if(snap) nwX = Math.floor((nwX+size/2) / size) * size;
					update = true;
				}
				
				float dy = y - pressOffsetY;
				if(!lockY && dy!=0) {
					nwY += dy*parent.getCamera().dZoom;
					if(snap) nwY = Math.floor((nwY+size/2) / size) * size;
					update = true;
				}
				
				if(update) {
					String str = Lissajous.doubleToString(worldToFunctionValue(nwX, nwY));
					textfield.setText(str);
				}
			}
		}
		
	}
	
}
