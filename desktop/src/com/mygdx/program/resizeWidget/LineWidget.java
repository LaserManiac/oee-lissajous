package com.mygdx.program.resizeWidget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Align;
import com.mygdx.program.signalScreen.NumericTextfield;

public abstract class LineWidget extends ResizeWidget {

	public LineWidget(NumericTextfield textfield, Color color, Axis axis) {
		super(textfield, color);
		lockX = axis == Axis.Y;
		lockY = !lockX;
	}

	@Override
	public void align(float x, float y) {
		setSize(lockX ? getParent().getWidth() : 4, lockY ? getParent().getHeight() : 4);
		setPosition(lockX ? getParent().getWidth()/2 : x, lockY ? getParent().getHeight()/2 : y, Align.center);
	}
	
	@Override
	public void drawShape(ShapeRenderer renderer) {
		int len = 8;
		if(lockX) {
			float x = getParent().getX() + getX();
			float y = getParent().getY() + getY() + getHeight()/2;
			float width = getParent().getWidth();
			for(int i = -len/2; i <= width; i+= len*2) renderer.line(x+i, y, x+i+len, y);
		} else {
			float x = getParent().getX() + getX() + getWidth()/2;
			float y = getParent().getY() + getY();
			float height = getParent().getHeight();
			for(int i = -len/2; i <= height; i+= len*2) renderer.line(x, y+i, x, y+i+len);
		}
	}
	
	public static enum Axis {
		X, Y;
	}
	
}
