package com.mygdx.program.resizeWidget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Align;
import com.mygdx.program.signalScreen.NumericTextfield;

public abstract class CircleWidget extends ResizeWidget {

	public CircleWidget(NumericTextfield textfield, Color color) {
		super(textfield, color);
	}

	@Override
	public void align(float x, float y) {
		setPosition(x, y, Align.center);
	}
	
	@Override
	public void drawShape(ShapeRenderer renderer) {
		renderer.circle(getParent().getX() + getX() + getWidth()/2, getParent().getY() + getY() + getHeight()/2, 8);
	}
	
}
